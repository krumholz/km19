"""
This module carries out the analysis of the observed eps_ff distribution
"""

# Libraries
import numpy as np
import multiprocessing
import matplotlib.pyplot as plt
import astropy.io.ascii as asc
import astropy.io.fits as fits
import astropy.units as u
import os.path as osp
import emcee
from npdfplot import npdfplot
from utils import models, eff_dist
from scipy.special import gamma

#############################################################

# Import data

# Data directory
datadir = 'data'

# Read in Heyer+ 2016 data -- first detections
hdulist = fits.open(osp.join(datadir, 'heyer2016_sf.fits'))
epsff_lo_heyer2016 = np.maximum(hdulist[1].data['SigmaSFR'],0.5) / \
                     (hdulist[1].data['SigmaH2']/hdulist[1].data['tauff'])
epsff_hi_heyer2016 = epsff_lo_heyer2016 * hdulist[1].data['Mcorr']
epsff_heyer2016_sf = np.sqrt(epsff_lo_heyer2016*epsff_hi_heyer2016)
tff1 = hdulist[1].data['tauff']
hdulist.close()

# Now read non-detections
hdulist = fits.open(osp.join(datadir, 'heyer2016_nosf.fits'))
epsff_ulim_heyer2016 = np.maximum(hdulist[1].data['SigmaSFRlim'], 0.5) / \
                       (hdulist[1].data['SigmaH2']/hdulist[1].data['tauff'])
idx = epsff_lo_heyer2016 > 0
mean_corr = np.mean(epsff_hi_heyer2016[idx]/epsff_lo_heyer2016[idx])
epsff_obs = np.concatenate([epsff_heyer2016_sf,
                            mean_corr*epsff_ulim_heyer2016])
tff2 = hdulist[1].data['tauff']
tff = np.concatenate([tff1, tff2])
hdulist.close()

# Sort list of eps_ff values for convenience of plotting later
epsff_obs.sort()

# Set time values
tff_obs = np.mean(tff)
dt_obs = 0.5

# Set survey gas to star ratio limit parameter
mg_ms = [ 0.1, 1.0, 10.0 ]

#############################################################

# Set up list of models that we will be evaluating, with chosen
# parameters and limits for each
model_names = [ "ST", "CBp0", "CB", "CBD", "GC", "GCD", "IE"]

# Parameters we fix
fixed_pars = {
    "ST"  :  { },
    "CB"  :  { "p" : 3 },
    "CBp0" : { "p" : 0 },
    "CBD" :  { "p" : 3 },
    "GC"  :  { },
    "GCD" :  { },
    "IE"  :  { }
}

# Limits we will enforce on priors for particular models
par_limits = {
    "all"  : { "eta_lim"    : [ 0.01, 10.],
               "sigma_lim"  : [ 0.01, 10.],
               "eff_lim"   : [ 1.0e-4, 1.] },
    "ST"   : { },
    "CB"   : { "t_acc_lim"  : [ 0.01, 100.] },
    "CBp0" : { "t_acc_lim"  : [ 0.01, 100.] },
    "CBD"  : { "t_acc_lim"  : [ 0.01, 100.], 
               "phi_d_lim"  : [ 1.0, 100.] },
    "GC"   : { "t_coll_lim" : [ 0.01, 100.],
               "xi_lim"     : [ 0.1, 10.] },
    "GCD"  : { "t_coll_lim" : [ 0.01, 100.],
               "t_fb_lim"   : [ 0.01, 100.],
               "phi_d_lim"  : [ 1.0, 100.],
               "xi_lim"     : [ 0.1, 10.] },
    "IE"   : { "delta_lim"  : [ 0.0, 3.0] }
}

# Starting values for the MCMC; these don't really matter, they just
# need to be inside the range of allowable priors
par_start = {
    "all"  : [ 0.0, 0.5, 1.0],  # log sigma, log eta, log t_sf
    "ST"   : [ ],                # No other parameters
    "CB"   : [ 1.0 ],            # log t_acc
    "CBp0" : [ 1.0 ],            # log t_acc
    "CBD"  : [ 1.0, 1.0 ],       # log t_acc, log phi_d
    "GC"   : [ 0.0, 0.0 ],       # log t_coll, log xi
    "GCD"  : [ 0.0, 0.0,
               -0.5, 1.0 ],      # log t_coll, log xi, log t_fb, log phi_d
    "IE"   : [ 0.1 ]             # delta
}

# Pack model info
model_params = { }
for mname in model_names:
    model_params[mname] = { "par_limits" : par_limits[mname],
                            "par_start"  : par_start[mname],
                            "fixed_pars" : fixed_pars[mname]
    }


#############################################################

# Set up the function that returns the log likelihood; this is what
# emcee will call

def lnL_eff_dist(theta, eff_obs, dt, t_ff, mg_ms, name, par_limits,
                 fixed_pars):

    # Unpack input parameters that are the same for all models
    sigma = 10.**theta[0]
    eta = 10.**theta[1]
    t_sf = 10.**theta[2]

    # Enforce limits
    if sigma < par_limits["all"]["sigma_lim"][0] or \
       sigma > par_limits["all"]["sigma_lim"][1] or \
       eta < par_limits["all"]["eta_lim"][0] or \
       eta > par_limits["all"]["eta_lim"][1]:
        return -np.inf
    eff_lim = par_limits["all"]["eff_lim"]

    # Unpack the model-dependent input parameters, if any; enforce
    # limits, and generate parameter list to be passed to eff_dist
    # function
    if name == "ST":
        eff = t_ff / ((1.0+eta) * t_sf)
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        pars = (eta,) # eta
        modname = "ST"
    elif name == "CB" or name == "CBp0":
        eff = t_ff / ((1.0+eta) * t_sf)
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        t_acc = 10.**theta[3]
        if t_acc < par_limits[name]["t_acc_lim"][0] or \
           t_acc > par_limits[name]["t_acc_lim"][1]:
            return -np.inf
        if t_acc < t_ff: return -np.inf
        pars = (eta,
                fixed_pars[name]["p"],
                t_acc/t_sf)  # eta, p, tau_acc
        modname = "CB"
    elif name == "CBD":
        eff = t_ff / ((1.0+eta) * t_sf)
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        t_acc = 10.**theta[3]
        phi_d = 10.**theta[4]
        if t_acc < par_limits[name]["t_acc_lim"][0] or \
           t_acc > par_limits[name]["t_acc_lim"][1] or \
           phi_d < par_limits[name]["phi_d_lim"][0] or \
           phi_d > par_limits[name]["phi_d_lim"][1]: 
            return -np.inf
        if t_acc < t_ff: return -np.inf
        pars = (eta,
                fixed_pars[name]["p"],
                t_acc/t_sf,
                (1.0+eta)*phi_d - 1.0)  # eta, p, tau_acc, eta_d
        modname = "CBD"
    elif name == "GC":
        t_coll = 10.**theta[3]
        xi = 10.**theta[4]
        if t_coll < par_limits[name]["t_coll_lim"][0] or \
           t_coll > par_limits[name]["t_coll_lim"][1]: 
            return -np.inf
        if xi < par_limits[name]["xi_lim"][0] or \
           xi > par_limits[name]["xi_lim"][1]:
            return -np.inf
        t_ff0 = xi/2. * t_coll
        if t_ff0 < t_ff:
            return -np.inf
        eff = t_ff0 / ((1.0+eta)*t_sf)
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        pars = (eta, 
                t_coll/t_sf) # eta, tau_coll
        modname = "GC"
    elif name == "GCD":
        t_coll = 10.**theta[3]
        xi = 10.**theta[4]
        t_fb = 10.**theta[5]
        phi_d = 10.**theta[6]
        if t_coll < par_limits[name]["t_coll_lim"][0] or \
           t_coll > par_limits[name]["t_coll_lim"][1]: 
            return -np.inf
        if t_fb < par_limits[name]["t_fb_lim"][0] or \
           t_fb > t_coll: 
            return -np.inf
        if phi_d < par_limits[name]["phi_d_lim"][0] or \
           phi_d > par_limits[name]["phi_d_lim"][1]:
            return -np.inf
        if xi < par_limits[name]["xi_lim"][0] or \
           xi > par_limits[name]["xi_lim"][1]:
            return -np.inf
        t_ff0 = xi/2. * t_coll
        if t_ff0 < t_ff:
            return -np.inf
        eff = t_ff0 / ((1.0+eta)*t_sf)
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        pars = (eta, 
                t_coll/t_sf,
                t_fb/t_sf,
                phi_d)       # eta, tau_coll, tau_fb, phi_d
        modname = "GCD"
    elif name == "IE":
        delta = theta[3]
        if delta < par_limits[name]["delta_lim"][0] or \
           delta > par_limits[name]["delta_lim"][1]:
            return -np.inf
        chi = t_sf/t_ff
        eff0 = t_ff / ((1.0+eta) * t_sf)
        eff = delta/((1.+delta)**(1./(1.+delta))) * \
              gamma(delta/(1.+delta)) * \
              chi**(delta/(1.+delta)) * eff0
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        pars = (eta, 
                delta, 
                chi)  # eta, delta, chi
        modname = "IE"

    # Evaluate the PDF at each observed value of eps_ff; note that for
    # GC and GCD we divide by the current free-fall
    if name != "GC" and name != "GCD":
        res = eff_dist(eff_obs, dt/t_sf, t_ff/t_sf, pars, modname,
                       sigma)
    else:
        res = eff_dist(eff_obs, dt/t_sf, t_ff0/t_sf, pars, modname,
                       sigma)

    # Return final log likelihood
    lnL = np.sum(np.log(res))
    if np.isnan(lnL):
        import pdb; pdb.set_trace()
    return lnL


#############################################################

# Here is do the MCMC fits to the models

# Fixed parameters of the MCMC
nwalkers = 100
nsteps = 1000
nburn = 200

# Loop over models
mcmc = { }
for modname in model_params.keys():
    mod = model_params[modname]

    # Loop over M_gas / M_* values
    mcmc[modname] = {}
    for mratio in mg_ms:

        # See if we already have stored outputs for this model, and, if
        # so, read from disk instead of re-running
        fname = "mcmc_epsff_{:s}_mgms{:04.1f}.npz".format(modname, mratio)
        try:
            with np.load(osp.join("outputs", fname)) as data:
                mcmc[modname][str(mratio)] = {
                    "chains" : data["chains"],
                    "lnL"    : data["lnL"]
                }
            print(("Read data for model {:s}, M_gas/M_* limit = {:f} "
                   "from {:s}").format(modname, mratio, fname))
            continue
        except IOError:
            pass

        # Print status
        print("Fitting model {:s}, M_gas / M_* limit = {:f}".format(
            modname, mratio))

        # Set the number of dimensions and initial positions for
        # this model
        ndim = len(par_start["all"]) + len(mod["par_start"])
        initpos = [ np.array(par_start["all"] + mod["par_start"]) +
                    0.02*np.random.randn(ndim)
                    for i in range(nwalkers) ]

        # Set up the MCMC sampler
        sampler = emcee.EnsembleSampler(
            nwalkers, ndim, lnL_eff_dist,
            threads=multiprocessing.cpu_count(),
            args=(epsff_obs, dt_obs, tff_obs, mratio, modname,
                  par_limits, fixed_pars))

        # Run the MCMC
        d = sampler.run_mcmc(initpos, nsteps)

        # Store results
        mcmc[modname][str(mratio)] = {
            "chains" : sampler.chain,
            "lnL"    : sampler.lnprobability
        }

        # Write to file
        np.savez(osp.join("outputs", fname),
                 chains=sampler.chain,
                 lnL=sampler.lnprobability)

#############################################################

# Analysis section: here we compute various derived quantities and
# their statistical properties

# Loop over models and M_gas / M_*
for modname in model_params.keys():
    for mratio in mg_ms:

        # Extract samples
        m = mcmc[modname][str(mratio)]
        chains = m["chains"]
        ndim = chains.shape[-1]
        samples = chains[:,nburn:,:].reshape(-1,ndim)

        # Get percentiles on marginal posteriors of all parameters
        m["pct"] = []
        for n in range(ndim):
            m["pct"].append(
                np.percentile(samples[:,n], [16,50,84]))

        # Get eps_ff
        sigma = 10.**samples[:,0]
        eta = 10.**samples[:,1]
        t_sf = 10.**samples[:,2]
        if modname != "GC" and modname != "GCD" and modname != "IE":
            eff = tff_obs / ((1.0+eta)*t_sf)
        elif modname == "GC" or modname == "GCD":
            t_coll = 10.**samples[:,3]
            xi = 10.**samples[:,4]
            eff = t_coll * xi/2. / ((1.0+eta)*t_sf)
        elif modname == "IE":
            delta = samples[:,3]
            chi = t_sf / tff_obs
            eff0 = tff_obs / ((1.0+eta)*t_sf)
            eff = delta/((1+delta)**(1/(1+delta))) * \
                  gamma(delta/(1+delta)) * \
                  chi**(delta/(1+delta)) * eff0
        m["eff"] = eff
        m["eff_pct"] = np.percentile(eff, [16,50,84])


#############################################################

# Reporting section: here we print out a latex-formatted summary table
# of the results

# Print summary table in latex format
latex_names = ["ST", 
               "CB, $p=0$", 
               "CB, $p=3$", 
               "CBD, $p=3$", 
               "GC", 
               "GCD", 
               "IE"]
print("")
print("")
print("*************************************************************")
print("")
print("")

print("\\begin{tabular}{c@{\\qquad\\qquad}cccc@{\qquad\qquad}c}")
print("\\hline\\hline")
print("Model & \\multicolumn{4}{c}{Fit parameters} & "
      "Derived parameters \\\\")
print("     &   $\\log\\sigma_{{\log\eff}}$  &   $\\log\\eta$$^{{(a)}}$  &  "
      "$\\log\\tsf$  &   Other  &  "
      "$\log\\eff$$^{{(b)}}$  \\\\")
print("     &   [dex]                        &                 &  "
      "[Myr] \\\\ \\hline")

mratio = mg_ms[1]
for n, k in zip(latex_names, model_names):
    m = mcmc[k][str(mratio)]
    outstr = ""
    if k == "GCD":
        outstr += "\\multirow{2}{*}{"
    outstr += "{:s}  ".format(n)
    if k == "GCD":
        outstr += "}"
    npar = 3
    for i in range(npar):
        outstr += "&   "
        if k == "GCD":
            outstr += "\\multirow{2}{*}{"
        outstr += "${:4.2f}_{{{:+5.2f}}}^{{{:+5.2f}}}$  ".format(
            m["pct"][i][1],
            m["pct"][i][0]-m["pct"][i][1],
            m["pct"][i][2]-m["pct"][i][1]) 
        if k == "GCD":
            outstr += "}"
    if k == "ST":
        outstr += "&   "
        outstr += "--  "
    elif k == "CB" or k == "CBp0":
        outstr += "&    "
        outstr += ("$\\log\\ta = {:4.2f}_{{{:+5.2f}}}"
                   "^{{{:+5.2f}}}$   ").format(
                       m["pct"][npar][1],
                       m["pct"][npar][0]-m["pct"][npar][1],
                       m["pct"][npar][2]-m["pct"][npar][1])
    elif k == "CBD":
        outstr += "&    "
        outstr += ("$\\log\\ta = {:4.2f}_{{{:+5.2f}}}"
                   "^{{{:+5.2f}}}$, ").format(
                       m["pct"][npar][1],
                       m["pct"][npar][0]-m["pct"][npar][1],
                       m["pct"][npar][2]-m["pct"][npar][1])
        outstr += ("$\\log\\phid = {:4.2f}_"
                   "{{{:+5.2f}}}^{{{:+5.2f}}}$   ").format(
                       m["pct"][npar+1][1],
                       m["pct"][npar+1][0]-m["pct"][npar+1][1],
                       m["pct"][npar+1][2]-m["pct"][npar+1][1])
    elif k == "GC" or k == "GCD":
        outstr += "&    "
        outstr += ("$\\log\\tc = {:4.2f}_{{{:+5.2f}}}"
                   "^{{{:+5.2f}}}$, ").format(
                       m["pct"][npar][1],
                       m["pct"][npar][0]-m["pct"][npar][1],
                       m["pct"][npar][2]-m["pct"][npar][1])
        outstr += ("$\\log\\xi = {:4.2f}_{{{:+5.2f}}}"
                   "^{{{:+5.2f}}}$   ").format(
                       m["pct"][npar+1][1],
                       m["pct"][npar+1][0]-m["pct"][npar+1][1],
                       m["pct"][npar+1][2]-m["pct"][npar+1][1])
    elif k == "IE":
        outstr += ("&    $\\delta = {:4.2f}_{{{:+5.2f}}}"
                   "^{{{:+5.2f}}}$   ").format(
                       m["pct"][npar][1],
                       m["pct"][npar][0]-m["pct"][npar][1],
                       m["pct"][npar][2]-m["pct"][npar][1])
    outstr += "&    "
    if k == "GCD":
        outstr += "\\multirow{2}{*}{"
    outstr += "${:4.2f}_{{{:+5.2f}}}^{{{:+5.2f}}}$   ".format(
        np.log10(m["eff_pct"][1]),
        np.log10(m["eff_pct"][0]/m["eff_pct"][1]),
        np.log10(m["eff_pct"][2]/m["eff_pct"][1]))
    if k == "GCD":
        outstr += "} "
        outstr += "\\\\ [0.5ex]"
        outstr += "   & & & & "
        outstr += ("$\\log\\tfb = {:4.2f}_{{{:+5.2f}}}"
                   "^{{{:+5.2f}}}$, ").format(
                       m["pct"][npar+2][1],
                       m["pct"][npar+2][0]-m["pct"][npar+2][1],
                       m["pct"][npar+2][2]-m["pct"][npar+2][1])
        outstr += ("$\\log\\phid = {:4.2f}_"
                   "{{{:+5.2f}}}^{{{:+5.2f}}}$   ").format(
                       m["pct"][npar+3][1],
                       m["pct"][npar+3][0]-m["pct"][npar+3][1],
                       m["pct"][npar+3][2]-m["pct"][npar+3][1])
    outstr += "\\\\ [1.5ex]"
    print(outstr)

print("\\hline\\hline")
print("\\end{tabular}")


#############################################################

# Plotting section

# Make plots of observed eps_ff distribution compared to model
# predictions
plt.figure(1, figsize=(7,2.5))
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.clf()

# List of models to plot, and their labels
cases_to_plot = [["ST", "CBp0"], ["CB", "CBD"], ["GC", "GCD", "IE"]]
plot_labels = { "ST" : "ST", 
                "CBp0" : r"CB, $p=0$",
                "CB" : r"CB, $p=3$", 
                "CBD" : r"CBD, $p=3$",
                "GC" : "GC",
                "GCD": "GCD",
                "IE" : "IE" }
mratio = str(mg_ms[1])

# General setup
efflim = [-3.5, -0.5]
ylim = [0, 1.7]
logeff = np.linspace(efflim[0], efflim[1], 500)

# Loop over plot panels
ctr = 0
for i in range(len(cases_to_plot)):
    
    # Add panel
    plt.subplot(1, len(cases_to_plot), i+1)

    # Plot observations
    if i == 0: lab = "Observed"
    else: lab = None
    n,bins,patches = plt.hist(np.log10(epsff_obs),
                              density=True, color='k', alpha=0.25,
                              bins="auto", label=lab)

    # Plot models
    for k in cases_to_plot[i]:
        for w in range(0,nwalkers,5):

            # Extract model prediction
            m = mcmc[k][str(mratio)]
            chains = m["chains"]
            sigma = 10.**chains[w,-1,0]
            eta = 10.**chains[w,-1,1]
            t_sf = 10.**chains[w,-1,2]
            if k == "ST":
                pars = (eta,)
                modname = "ST"
            elif k == "CB" or k == "CBp0":
                t_acc = 10.**chains[w,-1,3]
                pars = (eta, fixed_pars[k]["p"],
                        t_acc/t_sf)
                modname = "CB"
            elif k == "CBD":
                t_acc = 10.**chains[w,-1,3]
                phi_d = 10.**chains[w,-1,4]
                pars = (eta, fixed_pars[k]["p"],
                        t_acc/t_sf, 
                        (1.0+eta)*phi_d - 1.0)
                modname = "CBD"
            elif k == "GC":
                t_coll = 10.**chains[w,-1,3]
                xi = 10.**chains[w,-1,4]
                t_ff0 = xi/2. * t_coll
                pars = (eta, t_coll/t_sf)
                modname = "GC"
            elif k == "GCD":
                t_coll = 10.**chains[w,-1,3]
                xi = 10.**chains[w,-1,4]
                t_fb = 10.**chains[w,-1,5]
                phi_d = 10.**chains[w,-1,6]
                t_ff0 = xi/2. * t_coll
                pars = (eta, t_coll/t_sf, t_fb/t_sf, phi_d)
                modname = "GCD"
            elif k == "IE":
                delta = chains[w,-1,3]
                chi = t_sf / tff_obs
                pars = (eta, delta, chi)
                modname = "IE"
            if k != "GC" and k != "GCD":
                pdf = eff_dist(10.**logeff, dt_obs/t_sf, tff_obs/t_sf, pars,
                               modname, sigma)
            else:
                pdf = eff_dist(10.**logeff, dt_obs/t_sf, t_ff0/t_sf, pars,
                               modname, sigma)

            # Plot model
            plt.plot(logeff, pdf, 'C{:d}'.format(ctr), alpha=0.2)

            
            # Add dummy line for legend
            if w == 0:
                plt.plot([-1,-1], [-1,-1], 'C{:d}'.format(ctr),
                         label=plot_labels[k])

        ctr = ctr+1

        # Add legend and adjust limits and labels
        plt.legend(loc='upper left')
        plt.xlim(efflim)
        plt.ylim(ylim)
        plt.xlabel("$\log\,\epsilon_{\mathrm{ff,obs}}$")
        if i != 0: plt.gca().set_yticklabels([])
        else: plt.ylabel(r"$dp/d\log\,\epsilon_{\mathrm{ff,obs}}$")

# Final adjustments
plt.subplots_adjust(hspace=0.25, wspace=0.1, right=0.95, top=0.95,
                    left=0.1, bottom=0.18)
plt.savefig('eff_dist.pdf')


#############################################################

# Plot full posterior PDFs

plt.figure(2, figsize=(10,10))
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.clf()

# Labels to appears on axes
par_labels = [ r"$\log\,\sigma$ [dex]",
               r"$\log\,\eta$",
               r"$\log\,t_{\mathrm{sf}}$ [Myr]" ]
model_par_labels = { "ST"   : [],
                     "CB"   : [ r"$\log\,t_{\mathrm{acc}}$ [Myr]" ],
                     "CBp0" : [ r"$\log\,t_{\mathrm{acc}}$ [Myr]" ],
                     "CBD"  : [ r"$\log\,t_{\mathrm{acc}}$ [Myr]",
                                r"$\log\,\phi_{\mathrm{d}}$" ],
                     "GC"   : [ r"$\log\,t_{\mathrm{coll}}$ [Myr]",
                                r"$\log\,\xi$" ],
                     "GCD"  : [ r"$\log\,t_{\mathrm{coll}}$ [Myr]",
                                r"$\log\,\xi$",
                                r"$\log\,t_{\mathrm{fb}}$ [Myr]",
                                r"$\log\,\phi_{\mathrm{d}}$" ],
                     "IE"   : [ r"$\delta$" ] }
derived_par_labels = [ r"$\log\,\epsilon_{\mathrm{ff}}$" ]

# Loop over models
mratio = str(mg_ms[1])
for modname in model_names:

    # Extract samples
    m = mcmc[modname][str(mratio)]
    chains = m["chains"]
    ndim = chains.shape[-1]
    samples = chains[:,nburn:,:].reshape(-1,ndim)

    # Grab eps_ff
    logeff = np.log10(m["eff"])
    samples = np.hstack((samples, logeff.reshape(logeff.size,1)))

    # Set up labels
    labels = par_labels + model_par_labels[modname] + derived_par_labels

    # Make plot
    npdfplot(samples, fignum=2, labels=labels)
    plt.subplots_adjust(top=0.96, left=0.1, hspace=0.12, wspace=0.12,
                        bottom=0.08)

    # Save
    plt.savefig("corner_eff_"+modname+".pdf")

