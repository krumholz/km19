"""
This file defines some utility functions for modeling the formation of
star clusters
"""

import numpy as np
from scipy.special import factorial, erf, erfc, gamma
from scipy.ndimage import fourier_gaussian
from scipy.optimize import root_scalar
from quadpy.line_segment import integrate_adaptive, gauss_patterson
import warnings

# Utility value
small = np.finfo(np.double).tiny

# Set up quadrature calculators
gp4 = gauss_patterson(4)
gp5 = gauss_patterson(5)

# Define the g function that appears in the CB and CBD models
def gcb(tau,q):
    i = np.arange(1,q+1)
    tau_ = np.atleast_1d(tau)
    g = factorial(q)*(
        (-1)**q * np.exp(-tau_) -
        np.sum(np.transpose(
            (-1)**i * 
            np.power.outer(tau_,(q-i)) / 
            factorial(q-i)),
               axis=0))
    # Use a series expansion for small tau, to avoid numerical issues
    idx = tau_ < 0.1
    g[idx] = tau_[idx]**q * (1 - tau_[idx]/(q+1) + 
                        tau_[idx]**2/((q+1)*(q+2)) - 
                        tau_[idx]**3/((q+1)*(q+2)*(q+3)))
    return g


# Define models for dimensionless stellar mass, gas mass, and SFR vs
# dimensionless time. Here tau = dimensionless time, par = model
# parameters, name = name of model, ret = "g", "s", "r", or "all",
# meaning return the gas mass, stellar mass, rate of star formation,
# or all three.
def models(tau, pars, name, ret):
    """
    Return the stellar mass, gas mass, or SFR for a star formation
    model as a function of dimensionless time.

    Parameters
       tau : float or array
          dimensionless time
       pars : tuple
          tuple of model parameters; what is expected depends on the
          model: (eta,) for ST, (eta,p,tau_acc) for CB,
          (eta,p,tau_acc,phi_d) for CBD, (eta,tau_coll) for GC,
          (eta,tau_coll,tau_fb,phi_d) for GCD, (eta,delta,chi) for IE
       name : "ST" | "CB" | "CBD" | "GC" | "GCD" | "IE"
          name of model to use
       ret : "s" | "g" | "r" | "all"
          what should be returned: s = stellar mass, g = gas mass, r =
          star formation rate, all = tuple of all three

    Returns
       (s, g, r) : (optional) array
          either an array or a tuple of three arrays; contents depend
          on the value of ret
    """
    
    # See what we're returning
    tau_ = np.atleast_1d(tau)
    if ret == "s" or ret == "all":
        ms = np.zeros(tau_.shape)
    if ret == "g" or ret == "all":
        mg = np.ones(tau_.shape)
    if ret == "r" or ret == "all":
        sfr = np.zeros(tau_.shape)
    
    # Action depends on model; for each model, we extract the
    # parameters for that model from pars, then evaluate
    if name == "ST":
        
        # Extract parameters
        eta, = pars
        idx = tau_ >= 0
        if np.array(eta).ndim > 0:
            eta_ = eta[idx]
        else:
            eta_ = eta
        if ret == "s" or ret == "all":
            ms[idx] = (1.0 - np.exp(-tau_[idx])) / (1.0+eta_)
        if ret == "g" or ret == "all":
            mg[idx] = np.exp(-tau_[idx])
        if ret == "r" or ret == "all":
            sfr[idx] = np.exp(-tau_[idx]) / (1.0+eta_)
        
    elif name == "CB":
        
        # Extract parameters
        eta, p, tau_acc = pars
        
        # During accretion
        idx_acc = np.logical_and(tau_ >= 0, tau_ <= tau_acc)
        if np.array(eta).ndim > 0:
            eta_ = eta[idx_acc]
        else:
            eta_ = eta
        if np.array(tau_acc).ndim > 0:
            tau_acc_ = tau_acc[idx_acc]
        else:
            tau_acc_ = tau_acc
        if ret == "s" or ret == "all":
            ms[idx_acc] = tau_acc_**(-p-1) * \
                (tau_[idx_acc]**(p+1) - gcb(tau_[idx_acc],p+1)) / (1.0+eta_)
        if ret == "g" or ret == "all":
            mg[idx_acc] = tau_acc_**(-p-1) * gcb(tau_[idx_acc], p+1)
        if ret == "r" or ret == "all":
            sfr[idx_acc] = tau_acc_**(-p-1) * \
                           gcb(tau_[idx_acc], p+1) / (1.0+eta_)
            
        # After accretion
        idx_no_acc = tau_ > tau_acc
        if np.array(eta).ndim > 0:
            eta_ = eta[idx_no_acc]
        else:
            eta_ = eta
        if np.array(tau_acc).ndim > 0:
            tau_acc_ = tau_acc[idx_no_acc]
        else:
            tau_acc_ = tau_acc
        if ret == "s" or ret == "all":
            ms[idx_no_acc] = tau_acc_**(-p-1) * (
                tau_acc_**(p+1) - gcb(tau_acc_,p+1) + 
                gcb(tau_acc_,p+1) * 
                (1.0 - np.exp(-tau_[idx_no_acc]+tau_acc_))) / (1.0+eta_)
        if ret == "g" or ret == "all":
            mg[idx_no_acc] = tau_acc_**(-p-1) * gcb(tau_acc_, p+1) * \
                np.exp(-tau_[idx_no_acc]+tau_acc_)
        if ret == "r" or ret == "all":
            sfr[idx_no_acc] = tau_acc_**(-p-1) * gcb(tau_acc_, p+1) * \
                np.exp(-tau_[idx_no_acc]+tau_acc_) / (1.0+eta_)
            
    elif name == "CBD":
        
        # Extract parameters
        eta, p, tau_acc, etad = pars
        phid = (1.0+etad) / (1.0+eta)
        
        # During accretion
        idx_acc = np.logical_and(tau_ >= 0, tau_ <= tau_acc)
        if np.array(eta).ndim > 0:
            eta_ = eta[idx_acc]
        else:
            eta_ = eta
        if np.array(tau_acc).ndim > 0:
            tau_acc_ = tau_acc[idx_acc]
        else:
            tau_acc_ = tau_acc
        if np.array(phid).ndim > 0:
            phid_ = phid[idx_acc]
        else:
            phid_ = phid
        if ret == "s" or ret == "all":
            ms[idx_acc] = tau_acc_**(-p-1) * \
                (tau_[idx_acc]**(p+1) - gcb(tau_[idx_acc],p+1)) / (1.0+eta_)
        if ret == "g" or ret == "all":
            mg[idx_acc] = tau_acc_**(-p-1) * gcb(tau_[idx_acc], p+1)
        if ret == "r" or ret == "all":
            sfr[idx_acc] = tau_acc_**(-p-1) * \
                           gcb(tau_[idx_acc], p+1) / (1.0+eta_)
            
        # After accretion
        idx_no_acc = tau_ > tau_acc
        if np.array(eta).ndim > 0:
            eta_ = eta[idx_no_acc]
        else:
            eta_ = eta
        if np.array(tau_acc).ndim > 0:
            tau_acc_ = tau_acc[idx_no_acc]
        else:
            tau_acc_ = tau_acc
        if np.array(phid).ndim > 0:
            phid_ = phid[idx_no_acc]
        else:
            phid_ = phid
        if ret == "s" or ret == "all":
            ms[idx_no_acc] = 1.0/(1.0+eta_) * tau_acc_**(-p-1) * (
                tau_acc_**(p+1) - gcb(tau_acc_,p+1) + 
                (1.0/phid_) * gcb(tau_acc_,p+1) * 
                (1.0 - np.exp(-phid_*(tau_[idx_no_acc]-tau_acc_))))
        if ret == "g" or ret == "all":
            mg[idx_no_acc] = tau_acc_**(-p-1) * gcb(tau_acc_, p+1) * \
                np.exp(-phid_ * (tau_[idx_no_acc]-tau_acc_))
        if ret == "r" or ret == "all":
            sfr[idx_no_acc] = tau_acc_**(-p-1) * gcb(tau_acc_, p+1) * \
                np.exp(-phid_ * (tau_[idx_no_acc]-tau_acc_)) / (1.0+eta_)
            
    elif name == "GC":
        
        # Extract parameters
        eta, tau_coll = pars
        
        # Before collpase
        idx_coll = np.logical_and(tau_ >= 0, tau_ < tau_coll)
        if np.array(eta).ndim > 0:
            eta_ = eta[idx_coll]
        else:
            eta_ = eta
        if np.array(tau_coll).ndim > 0:
            tau_coll_ = tau_coll[idx_coll]
        else:
            tau_coll_ = tau_coll
        if ret == "s" or ret == "all":
            ms[idx_coll] = 1/(1.0+eta_) * \
                (1 - (1 - tau_[idx_coll]/tau_coll_)**tau_coll_)
        if ret == "g" or ret == "all":
            mg[idx_coll] = (1 - tau_[idx_coll]/tau_coll_)**tau_coll_
        if ret == "r" or ret == "all":
            sfr[idx_coll] \
                = (1 - tau_[idx_coll]/tau_coll_)**(tau_coll_-1) / (1.0+eta_)
            
        # After collapse
        idx_post_coll = tau_ >= tau_coll
        if np.array(eta).ndim > 0:
            eta_ = eta[idx_post_coll]
        else:
            eta_ = eta
        if ret == "s" or ret == "all":
            ms[idx_post_coll] = 1.0/(1.0+eta_)
        if ret == "g" or ret == "all":
            mg[idx_post_coll] = 0.0
        if ret == "r" or ret == "all":
            sfr[idx_post_coll] = 0.0
            
    elif name == "GCD":
        
        # Extract parameters
        eta, tau_coll, tau_fb, phi_d = pars

        # Before feedback increases
        idx1 = np.logical_and(tau_ >= 0, tau_ < tau_fb)
        if np.array(eta).ndim > 0:
            eta_ = eta[idx1]
        else:
            eta_ = eta
        if np.array(tau_coll).ndim > 0:
            tau_coll_ = tau_coll[idx1]
        else:
            tau_coll_ = tau_coll
        if ret == "s" or ret == "all":
            ms[idx1] = 1/(1.0+eta_) * \
                (1 - (1 - tau_[idx1]/tau_coll_)**tau_coll_)
        if ret == "g" or ret == "all":
            mg[idx1] = (1 - tau_[idx1]/tau_coll_)**tau_coll_
        if ret == "r" or ret == "all":
            sfr[idx1] \
                = (1 - tau_[idx1]/tau_coll_)**(tau_coll_-1) / (1.0+eta_)

        # After feedback increases, but before collapse
        idx2 = np.logical_and(tau_ >= tau_fb, tau_ < tau_coll)
        if np.array(eta).ndim > 0:
            eta_ = eta[idx2]
        else:
            eta_ = eta
        if np.array(tau_coll).ndim > 0:
            tau_coll_ = tau_coll[idx2]
        else:
            tau_coll_ = tau_coll
        if np.array(tau_fb).ndim > 0:
            tau_fb_ = tau_fb[idx2]
        else:
            tau_fb_ = tau_fb
        if np.array(phi_d).ndim > 0:
            phi_d_ = phi_d[idx2]
        else:
            phi_d_ = phi_d
        x = tau_[idx2]/tau_coll_
        xfb = tau_fb_ / tau_coll_
        if ret == "s" or ret == "all":
            ms[idx2] = 1/(1.0+eta_) * (
                1 - (1 - xfb)**tau_coll_ +
                (1 - xfb)**tau_coll_ / phi_d_ *
                (1 - ((1-x)/(1-xfb))**(phi_d_*tau_coll_)))
        if ret == "g" or ret == "all":
            mg[idx2] = (1 - xfb)**tau_coll_ * \
                       ((1-x)/(1-xfb))**(phi_d_*tau_coll_)
        if ret == "r" or ret == "all":
            sfr[idx2] = 1.0 / ((1-x)*(1+eta)) * \
                        (1 - xfb)**tau_coll_ * \
                        ((1-x)/(1-xfb))**(phi_d_*tau_coll_)
        
        # After collpase
        idx3 = tau_ >= tau_coll
        if np.array(eta).ndim > 0:
            eta_ = eta[idx3]
        else:
            eta_ = eta
        if np.array(tau_coll).ndim > 0:
            tau_coll_ = tau_coll[idx3]
        else:
            tau_coll_ = tau_coll
        if np.array(tau_fb).ndim > 0:
            tau_fb_ = tau_fb[idx3]
        else:
            tau_fb_ = tau_fb
        if np.array(phi_d).ndim > 0:
            phi_d_ = phi_d[idx3]
        else:
            phi_d_ = phi_d
        xfb = tau_fb_ / tau_coll_
        if ret == "s" or ret == "all":
            ms[idx3] = (1.0 + (1.0/phi_d_-1)*(1-xfb)**tau_coll_) / (1 + eta_)
        if ret == "g" or ret == "all":
            mg[idx3] = 0.0
        if ret == "r" or ret == "all":
            sfr[idx3] = 0.0
            
    elif name == "IE":
        
        # Extract parameters
        eta, delta, chi = pars
        idx = tau_ >= 0
        if np.array(eta).ndim > 0:
            eta_ = eta[idx]
        else:
            eta_ = eta
        if np.array(delta).ndim > 0:
            delta_ = delta[idx]
        else:
            delta_ = delta
        if np.array(chi).ndim > 0:
            chi_ = chi[idx]
        else:
            chi_ = chi
        if ret == "s" or ret == "all":
            ms[idx] = 1/(1.0+eta_) * \
                      (1.0 - np.exp(-chi_**delta_*tau_[idx]**(1+delta_) \
                              /(1+delta_)))
        if ret == "g" or ret == "all":
            mg[idx] = np.exp(-chi_**delta_ * tau_[idx]**(1+delta_) \
                             / (1+delta_))
        if ret == "r" or ret == "all":
            sfr[idx] = np.exp(-chi_**delta_ * tau_[idx]**(1+delta_) \
                              / (1+delta_)) * \
                              (chi_*tau_[idx])**delta_ / (1.0+eta_)
            
    else:
        
        raise ValueError("unknown model {:s}".format(name))
        
    # Return
    if ret == "s":
        return ms
    elif ret == "g":
        return mg
    elif ret == "r":
        return sfr
    elif ret == "all":
        return mg, ms, sfr
    else:
        raise ValueError("unknown return code {:s}".format(ret))


# Function to return the PDF or CDF of stellar ages for a given model,
# possibly convolved with an error distribution
def age_dist(tau_obs, tau_star, pars, name,
             sigma = 0.0, bias = 0.0, log = False,
             cumulative = False, initres = 1024, maxres = 32768,
             epsrel=1e-2, epsabs=1e-2, gc_trunc = 1.0):
    """
    Return the stellar age distribution (PDF or CDF) predicted by a
    particular model

    Parameters
       tau_obs : float
          time at which the observation takes place, where tau = 0 is
          the time when star formation begins
       tau_star : float or array
          stellar age(s) at which the PDF or CDF is to be evaluated
       pars : tuple
          tuple of model parameters; what is expected depends on the
          model: (eta,) for ST, (eta,p,tau_acc) for CB,
          (eta,p,tau_acc,phi_d) for CBD, (eta,tau_coll) for GC,
          (eta,tau_coll,tau_fb,phi_d) for GHC, (eta,delta,chi) for IE
       name : "ST" | "CB" | "CBD" | "GC" | "GCD" | "IE"
          name of model to use
       sigma : float
          dispersion of obsevational error distribution, in dex
       bias : float
          bias of observational error distribution, in dex
       log : bool
          if True, the returned PDF is dp / d log tau; if False, it is
          dp / dtau; if cumulative is True, this option has no
          effect. Note that the value returned is the PDF in log base
          10, not base e -- these two differ by a factor of ln(10)
       cumulative : bool
          if True, return the CDF rather than the PDF of ages
       initres : int
          initial resolution for the error convolution; only used if
          sigma > 0
       maxres : int
          maximum resolution at which SF history should be sampled; if
          desired accuracy goal cannot be achieved at this resolution,
          best available data will be returned but a warning will be
          issued; only used if sigma > 0
       epsrel : float
          relative accuracy goal for error-convolved age distribution
       epsabs : float
          absolute accuracy goal for error-convolved age distribution
       gc_trunc : float
          fraction of sigma to cut off and compute by direct sum in
          GC and GCD models; has no effect for any other models

    Returns
       dist : PDF or CDF (if cumulative = True) of stellar ages
    """

    if sigma == 0.0:
        
        # Handle special case sigma = 0, in which case we just return the
        # appropriately normalised SFR (for the PDF) or fraction of stellar
        # mass (for the CDF) at the input age with an appropriate bias applies
        tau_age = tau_obs - 10.**(-bias)*tau_star
        if cumulative:
            return 1.0 - models(tau_age, pars, name, "s") / \
                models(tau_obs, pars, name, "s")
        else:
            dpdtau = models(tau_age, pars, name, "r") / \
                     models(tau_obs, pars, name, "s")
            # Floor to avoid error messages from the MCMC solver
            dpdtau[dpdtau <= 0.0] = small
            if log:
                return dpdtau * tau_star
            else:
                return dpdtau
        
    else:

        # Case sigma != 0

        # Idiocy prevention
        if np.amin(tau_star) <= 0.0:
            raise ValueError("cannot evaluate for tau_star <= 0 "
                             "with sigma != 0")

        # For convenience, get error and bias in log e
        s = sigma*np.log(10)
        b = bias*np.log(10)

        # Set initial resolution of the array with which we will
        # sample the SF history
        res = initres

        # Iterate until convergence
        pdf_last = np.zeros(tau_star.shape)
        while res <= maxres:

            # This is the method we use for all models except GC and GCD;
            # see below for explanation of why

            # Construct grid of x values, being sure to go out far
            # enough from the input stellar age range that we can be
            # sure that any errors coming from parts of the SF history
            # outside the region we sample are negligibly small
            nsigma = -np.log(np.minimum(epsrel,epsabs)/4.)
            logtau0 = np.amin(np.log(tau_star)) - nsigma*s
            logtau1 = np.amax(np.log(tau_star)) + nsigma*s
            dlogtau = (logtau1 - logtau0) / res
            x = np.linspace(logtau0, logtau1, res) - b

            # Evaluate the function we will convolve to get dM_* /
            # dlog tau
            tau = tau_obs - np.exp(x + b)
            sfr_norm = models(tau, pars, name, "r") / \
                       models(tau_obs, pars, name, "s")
            func = np.exp(x+b) * sfr_norm

            # Special handling of the GC(D) model: these models can have
            # most of their star formation in a delta function-like
            # spike at tau = tau_coll, which creates problems with the
            # accuracy of the FFT-based convolution. To deal with
            # this, for this model we take the time interval from
            # tau_coll - gc_trunc*sigma to tau_coll and deal with it by a
            # direct sum; here we are just cutting out the part we'll
            # deal with later
            if name == "GC" or name == "GCD":
                eta = pars[0]
                tau_coll = pars[1]
                m_tot = models(tau_obs, pars, name, 's')[0]
                m_trunc = models(tau_coll-s*gc_trunc, pars, name, 's')[0]
                func[np.logical_and(tau < tau_coll,
                                    tau > tau_coll-gc_trunc*s)] = 0.0
                
            # Do the convolution
            func_ft = np.fft.rfft(func)
            func_ft_convol \
                = fourier_gaussian(func_ft, s/dlogtau, n=res)
            dm_dlogt = np.fft.irfft(func_ft_convol)

            # If computing CDF instead of PDF, sum the PDF, and add a
            # term representing stars formed before the start of our
            # sample region
            if cumulative:
                dm_dlogt \
                    = np.cumsum(dm_dlogt)*dlogtau + \
                    1.0 - \
                    models(tau_obs-np.exp(x[0]+b), pars, name, "s") / \
                    models(tau_obs, pars, name, "s")

            # Interpolate dm_dlogt to desired output times
            pdf = np.interp(np.log(tau_star), x+b, dm_dlogt)
           
            # Change to dp/dtau instead of dp/dlogtau if that is what
            # was requested
            if not cumulative and not log:
                pdf /= tau_star

            # Check for convergence; note that for GC(D) we're only
            # checking the contribution from part of the PDF at this
            # point, so we need to reduce the absolute error estimate
            abserr = np.abs(pdf - pdf_last)
            relerr = np.amax(abserr/(pdf+small))
            abserr = np.amax(abserr)
            if abserr < epsabs or relerr < epsrel:
                # Converged, so stop iterating
                break
            else:
                # Not converged, so store last iterate and double
                # resolution
                pdf_last = pdf
                res *= 2

    # Now handle the region we cut out in the GC and GCD models
    if name != "GC" and name != "GCD":
        # Flag that we don't have a cut-out region
        res_gc = 0
    elif m_trunc > m_tot:
        res_gc = 0
    else:

        # Iterate to convergence
        res_gc = initres / 4
        pdf_gc_last = np.zeros(tau_star.shape)
        while True:

            # Build a grid of sampling points to cover the period
            # we're cutting out
            m_sample = (1.0+eta)*np.linspace(m_trunc, m_tot, 5*res_gc)
            norm = (m_tot-m_trunc) / float(res_gc)
            tau_sample \
                = tau_coll * (1.0 - (1.0-m_sample)**(1.0/tau_coll))

            # Get the stellar ages for points we're handling directly,
            # and evaluate log(t_*,true) - log(t_*,obs) on the
            # grid of sample points
            tau_age = tau_obs - tau_sample
            tau_age[tau_age <= 0.0] = small  # Safety
            d_log_t = -np.subtract.outer(
                np.log(tau_age), np.log(tau_star+small)) - b

            # Evaluate PDF or CDF directly at output times
            if cumulative:
                p = 0.5 * (1 + erf(d_log_t/(2**0.5*s)))
            else:
                p = (1.0 / (np.sqrt(2*np.pi) * s) * \
                     np.exp(-d_log_t**2 / (2.0*s**2)))

            # Sum contributions over the sample grid using a 5 point
            # Newton-Cotes formula
            integ = norm/90. * (7*p[::5] + 32*p[1::5] + 12*p[2::5] + 
                                32*p[3::5] + 7*p[4::5])
            pdf_gc = np.sum(integ, axis=0) / m_tot

            # Change to dp/dtau instead of dp/dlogtau if that is what
            # was requested
            if not cumulative and not log:
                pdf_gc /= tau_star

            # Check for convergence
            abserr_gc = np.abs(pdf_gc - pdf_gc_last)
            relerr_gc = np.amax(abserr_gc/(pdf_gc+small))
            abserr_gc= np.amax(abserr_gc)
            if abserr_gc < epsabs or relerr_gc < epsrel:
                # Converged, so stop iterating
                break
            else:
                # Not converged, so store last iterate and double
                # resolution
                pdf_gc_last = pdf_gc
                res_gc *= 2

        # Add contribution from the cut-out part
        pdf += pdf_gc
        abserr += abserr_gc
                
    # If we stopped iterating because we hit the resolution limit,
    # issue a warning
    if res > maxres or res_gc > maxres:
        warnings.warn("unable to obtain error-convolved age distribution "
                      "at requested accuracy -- estimated "
                      "(absolute, relative) error = {:e}, {:e}".format(
                          np.amax(abserr), np.amax(abserr/pdf)))
                    
    # Enforce pdf > 0; negative values are possible due to
    # numerical ringing in the FFT, but since we may take the
    # log, we want to ensure that we don't produce errors
    pdf[pdf <= 0.0] = small

    # If returning the dp / dlog(tau), multiply by ln(10) so that we
    # return dp / dlog_10(tau), not dp/dln(tau)
    if log and not cumulative:
        pdf *= np.log(10)
            
    # Return final estimate
    return pdf

# Helper function that returns M_gas / M_* - a constant and its
# derivative for any of the models; this is used to solve for times
# when the gas to star ratio reaches certain values in models where
# the solution cannot be written down exactly analytically
def mg_over_ms_derivs(tau, pars, name, mg_ms_target):

    # Call the model
    mg, ms, msp = models(tau, pars, name, 'all')

    # Compute dM_g / dtau
    mgp = -msp
    if name == "CB" or name == "CBD":
        # For these models, must add contribution from accretion
        tau_acc = pars[2]
        if tau < tau_acc:
            p = pars[1]
            mgp += (p+1) * (tau/tau_acc)**p / tau_acc

    # Return M_gas / M_* and its derivative
    if ms > 0.0:
        mg_ms = mg/ms
        mg_ms_p = mgp/ms - mg*msp/ms**2
    else:
        mg_ms = np.finfo(dtype='float').max
        mg_ms_p = np.finfo(dtype='float').min
    return mg_ms - mg_ms_target, mg_ms_p

# Function to return the time at which a given model reaches a
# specified ratio M_gas / M_*
def t_mg_ms(mg_ms, pars, name):
    """
    Return the dimensionless time at which M_gas / M_* has the
    specified value for a particular model

    Parameters
       mg_ms : float
          target value of M_gas / M_*
       pars : tuple
          tuple of model parameters; what is expected depends on the
          model: (eta,) for ST, (eta,p,tau_acc) for CB,
          (eta,p,tau_acc,phi_d) for CBD, (eta,tau_coll) for GC,
          (eta,tau_coll,tau_fb,phi_d) for GCD, and (eta,delta,chi) for IE
       name : "ST" | "CB" | "CBD" | "GHC" | "IE"
          name of model to use

    Returns
       tau : float
          time at which M_gas / M_* reaches the desired value
    """

    # Handle analytic cases
    eta = pars[0]
    if name == "ST":
        return np.log((1.0 + mg_ms + eta) / mg_ms)
    elif name == "GC":
        tau_coll = pars[1]
        return tau_coll * (
            1.0 - (mg_ms / (1.0 + mg_ms + eta))**(1.0/tau_coll))
    elif name == "IE":
        delta = pars[1]
        chi = pars[2]
        return ((1.0+delta) * chi**(-delta) * np.log(
            (1.0 + mg_ms + eta) / mg_ms))**(1.0/(1.0+delta))
    else:
        # For CB, CBD, and GCD models with arbitrary p we have to find the
        # root numerically
        if name == "CB" or name == "CBD":
            p = pars[1]
            tau_acc = pars[2]
            bracket = [0.0, (p+2)*(1.0+eta) / mg_ms]
        else:
            tau_coll = pars[1]
            bracket = [0.0, tau_coll]
        res = root_scalar(mg_over_ms_derivs,
                          args=(pars, name, mg_ms),
                          bracket=bracket,
                          fprime=True)
        if not res.converged:
            warnings.warn(root)
        return res.root

    
# Helper class: this class provides a function returns the value of
# the PDF or CDF evaluated at a particular time tau at a particular
# set of observed epsilon_ff values for a particular set of models. It
# is intended for use with quadpy
class eff_integrand(object):
    def __init__(self, dtau, tau_ff, log_eff, s, pars, name,
                 cumulative, tau_tot):
        self.dtau = dtau
        self.tau_ff = tau_ff
        self.log_eff = log_eff
        self.s = s
        self.pars = pars
        self.name = name
        self.cumulative = cumulative
        self.tau_tot = tau_tot
    def func(self, tau):
        ms = models(tau, self.pars, self.name, "s")
        ms1 = models(tau-self.dtau, self.pars, self.name, "s")
        mg = models(tau, self.pars, self.name, "g")
        if self.name != "GC" and self.name != "GCD":
            tau_ff = self.tau_ff
        else:
            # For GC and GCD, tau_ff is non-constant, so we need to use the
            # instantaneous value; the value we have stored is tau_ff,0
            tau_coll = self.pars[1]
            tau_ff = self.tau_ff * (1.0 - tau/tau_coll)
        log_eff_true = np.log10(((ms-ms1) / self.dtau) /
                                (mg / tau_ff))
        dlog_eff = np.subtract.outer(self.log_eff, log_eff_true)
        if self.cumulative:
            f = 0.5 * erfc(-dlog_eff/(2.**0.5*self.s)) / self.tau_tot
        else:
            f = 1.0/(np.sqrt(2.0*np.pi)*self.s) * \
                np.exp(-dlog_eff**2 / (2. * self.s**2)) / self.tau_tot
        # Impose floor on returned values to help integrator converge
        f[f < small] = small
        return f

    
# Function to return the distribution of epsilon_ff values observed
# for a particular star formation model
def eff_dist(eff, dtau, tau_ff, pars, name, sigma,
             mg_ms=1.0, tau_lim=None, cumulative=False,
             epsabs=1.0e-3, epsrel=1.0e-2, max_divisions=256):
    """
    Return the distribution of observed epsilon_ff values for a given
    model

    Parameters
       eff : float
          set of espilon_ff values at which to compute the PDF / CDF
       dtau : float
          dimensionless time interval over which YSOs are observable
       tau_ff : float
          dimensionless free-fall time; note that for the GHC model
          this will be interpreted as tau_ff,0, the free-fall time at
          the onset of collapse
       pars : tuple
          tuple of model parameters; what is expected depends on the
          model: (eta,) for ST, (eta,p,tau_acc) for CB,
          (eta,p,tau_acc,phi_d) for CBD, (eta,tau_coll) for GC,
          (eta,tau_coll,tau_fb,phi_d) for GCD, (eta,delta,chi) for IE
       name : "ST" | "CB" | "CBD" | "GC" | "GCD" | "IE"
          name of model to use
       sigma : float
          dispersion of obsevational error distribution, in dex
       mg_ms : float
          distribution is computed over the time interval for which
          M_gas / M_* >= mg_ms
       tau_lim : arraylike (2)
          values for start and stop time in integral; if this is set,
          the input value of mg_ms is ignore, and these times are used
          instead
       cumulative : bool
          if True, return the CDF rather than the PDF of ages
       eps : float
          accuracy goal
       max_divisions : int
          maximum number of subdivisions of the interval to attempt

    Returns
       dist : PDF or CDF (if cumulative = True) of epsilon_ff values
    """

    # Sanity check
    if sigma < 0.0:
        raise ValueError("sigma must be >= 0")
    elif sigma == 0.0 and not cumulative:
        raise ValueError("sigma must be > 0 to compute PDF")

    # Find the time interval to use given the input M_gas / M_*
    # value
    if tau_lim is not None:
        tau0 = tau_lim[0]
        tau1 = tau_lim[1]
    else:
        tau0 = 0.0
        tau1 = t_mg_ms(mg_ms, pars, name)

    # Do some preliminary computations
    log_eff_obs = np.log10(eff)
    s = np.log(10.0) * sigma

    # Create the helper class for numerical integration
    eff_int = eff_integrand(dtau, tau_ff, log_eff_obs, s, pars, name,
                            cumulative, tau1-tau0)

    # Set initial interval, and get estimates of integral using 4th
    # order and 5th order Gauss-Patterson schemes
    a = tau0
    b = tau1
    int4 = gp4.integrate(eff_int.func, [a, b])
    int5 = gp5.integrate(eff_int.func, [a, b])
    abserr = np.abs(int4 - int5)
    relerr = abserr / np.abs(int5)

    # Check if error is acceptable; if not, start adaptive calculation
    if np.amax(relerr) < epsrel or np.amax(abserr) < epsabs:
        return int5
    else:

        # Push first interval onto list
        interval_list = np.array([ [a,b] ])
        interval_err = np.array([ abserr ])
        interval_maxerr = np.array([ np.amax(abserr) ])
        interval_est = np.array([ int5 ])
        estimate = int5

        # Begin loop
        while interval_list.shape[0] < max_divisions:

            # Find interval with largest error
            idx = np.argmax(interval_maxerr)
            a, b = interval_list[idx]

            # Divide interval in half, and calculate estimate and
            # error on each half
            m = 0.5*(a+b)
            int4_l = gp4.integrate(eff_int.func, [a, m])
            int5_l = gp5.integrate(eff_int.func, [a, m])
            abserr_l = np.abs(int4_l - int5_l)
            int4_r = gp4.integrate(eff_int.func, [m, b])
            int5_r = gp5.integrate(eff_int.func, [m, b])
            abserr_r = np.abs(int4_r - int5_r)

            # Update overall estimate and error estimate
            estimate = estimate - interval_est[idx] + int5_l + int5_r
            abserr = abserr - interval_err[idx] + abserr_l + abserr_r

            # Check for convergence
            relerr = abserr / np.abs(estimate)
            if np.amax(relerr) < epsrel or np.amax(abserr) < epsabs:
                break

            # If we're here, we haven't converged yet. Replace values
            # for this interval in the list with the left half of the
            # integral we just computed
            interval_list[idx] = np.array([a, m])
            interval_err[idx] = abserr_l
            interval_maxerr[idx] = np.amax(abserr_l)
            interval_est[idx] = int5_l

            # Add interval to end of list
            interval_list = np.vstack((interval_list, [m,b]))
            interval_err = np.vstack((interval_err, abserr_r))
            interval_maxerr = np.vstack((interval_maxerr,
                                         np.amax(abserr_r)))
            interval_est = np.vstack((interval_est, int5_r))

    # Warn on convergence failure
    if interval_list.shape[0] == max_divisions:
        warnings.warn("Could not reach request tolerance in "
                      "eff_dist; estimate (abs, rel) error = "
                      "({:f}, {:f})".format(np.amax(relerr),
                                            np.amax(abserr)))

    if np.sum(estimate) == 0.0 or \
       np.any(np.logical_not(np.isfinite(estimate))):
        import pdb; pdb.set_trace()

    # Return
    return estimate

