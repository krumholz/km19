"""
This script generates the example model plots
"""

# Import libraries
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
from utils import models

# Plot style preferences
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

# Make example plots of various models

# Model parameter choices
pars = {
    "ST"  : (1.0,),             # eta
    "CB"  : (1.0, 3, 1.5),      # eta, p, tau_acc
    "CBD" : (1.0, 3, 1.5, 5.0), # eta, p, tau_acc, eta_d
    "GC"  : (1.0, 0.75),        # eta, tau_coll
    "GCD" : (1.0, 0.75, 0.5, 3.0), # eta, tau_coll, tau_fb, phi_d
    "IE"  : (1.0, 1.0, 0.5)     # eta, delta, chi
}
names = ["ST", "CB", "CBD", "GC", "GCD", "IE"]
ls = ["-", "--", ":", "-.", (0, (9,1,1,1,1,1)), (0, (3,1,1,1,1,1))]

# Start plot
fig1 = plt.figure(1, figsize=(4,8))
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
tau = np.linspace(0, 2.5, 500)

# Stellar panel
plt.subplot(3,1,1)
for i, n in enumerate(names):
    plt.plot(tau, models(tau, pars[n], n, "s"),
             color='C{:d}'.format(i), ls=ls[i], label=n)
plt.legend(ncol=3)
plt.gca().set_xticklabels([])
plt.xlim([0,2.5])
plt.ylim([0,0.7])
plt.ylabel(r'$M_*/M_{\mathrm{g,0}}$')

# Gas panel
plt.subplot(3,1,2)
for i, n in enumerate(names):
    plt.plot(tau, models(tau, pars[n], n, "g"),
             color='C{:d}'.format(i), ls=ls[i], label=n)
plt.gca().set_xticklabels([])
plt.ylim([0,1])
plt.xlim([0,2.5])
plt.ylabel(r'$M_\mathrm{g}/M_{\mathrm{g,0}}$')

# SFR panel
plt.subplot(3,1,3)
for i, n in enumerate(names):
    plt.plot(tau, models(tau, pars[n], n, "r"),
             color='C{:d}'.format(i), ls=ls[i], label=n)
plt.xlim([0,2.5])
plt.ylim([0,1.0])
plt.xlabel(r'$\tau$')
plt.ylabel(r'$(dM_*/d\tau)/M_{\mathrm{g,0}}$')

# Final adjustments and save
plt.subplots_adjust(hspace=0.1, left=0.15, right=0.95, top=0.95, bottom=0.1)
plt.savefig('example_models.pdf')



# Examples in physical units, using best-fit parameters

# Constants in cgs
G = const.G*1e3
Myr = 1e6*365.24*24.*3600.
mH = const.proton_mass*1e3

# Model parameter choices for two favoured models
epsff_best_fit = 10.**-1.75
mstar_norm = 2.0e3
tff_best_fit = {
    "CBD" : 0.3,
    "GCD" : 10.0
}
pars_best_fit = {
    "CBD" : (3.0, 3, 3.0, 39.),   # eta, p, tau_acc, eta_d
    "GCD" : (1.0, 0.04, 0.036, 10.) # eta, tau_coll, tau_fb, phi_d
}
names_best_fit = [ "CBD", "GCD" ]

# Draw figure
fig2 = plt.figure(2, figsize=(4,8))
t = np.linspace(0,15,2000)

# Loop over models
ax = []
for i, n in enumerate(names_best_fit):
    eta = pars_best_fit[n][0]
    tsf = tff_best_fit[n] / ((1.0+eta)*epsff_best_fit)
    tau = t/tsf
    mgas = models(tau, pars_best_fit[n], n, 'g')
    mstar = models(tau, pars_best_fit[n], n, 's')
    sfr = models(tau, pars_best_fit[n], n, 'r')
    norm = mstar_norm/mstar[-1]
    mgas = mgas * norm
    mstar = mstar * norm
    sfr = sfr * norm/(1e6*tsf)
    if n != "GCD":
        tff = np.ones(t.size)*tff_best_fit[n]
    else:
        tau_coll = pars_best_fit[n][1]
        xi = 2.0 * (1.0+eta) * epsff_best_fit / tau_coll
        tff = tff_best_fit[n] * (1.0 - 0.5*xi*t/tff_best_fit[n])
        tff[tff < 0] = np.nan

    # Plot model stellar mass
    if len(ax) < 1:
        ax.append(plt.subplot(4,1,1))
    else:
        plt.subplot(ax[0])
    plt.plot(t, mstar, color='C{:d}'.format(i), label=n)

    # Plot model gas mass
    if len(ax) < 2:
        ax.append(plt.subplot(4,1,2))
    else:
        plt.subplot(ax[1])
    plt.plot(t, mgas, color='C{:d}'.format(i), label=n)

    # Plot model SFR
    if len(ax) < 3:
        ax.append(plt.subplot(4,1,3))
    else:
        plt.subplot(ax[2])
    plt.plot(t, sfr, color='C{:d}'.format(i), label=n)

    # Plot model density / free-fall time
    if len(ax) < 4:
        ax.append(plt.subplot(4,1,4))
    else:
        plt.subplot(ax[3])
    plt.subplot(4,1,4)
    plt.plot(t, tff, color='C{:d}'.format(i), label=n)

# Adjust axes, spacing, etc.
ax[0].legend()
tlim = np.array([0,15])
for a in ax:
    a.set_xlim(tlim)
for a in ax[:-1]:
    a.set_xticklabels([])
ax[0].set_ylabel(r'$M_*$ [M$_\odot$]')
ax[0].set_ylim([0,2200])
ax[1].set_ylabel(r'$M_\mathrm{g}$ [M$_\odot$]')
ax[1].set_ylim([0,2.7e4])
ax[2].set_ylabel(r'$\dot{M}_*$ [M$_\odot$ yr$^{-1}$]')
ax[3].set_ylabel(r'$t_\mathrm{ff}$ [Myr]')
ax[2].set_yscale('log')
ax[2].set_ylim([1e-5,1e-3])
ax[3].set_yscale('log')
ax[3].set_xlabel(r'$t$ [Myr]')
tfflim = np.array([0.1, 10])
ax[3].set_ylim(tfflim)

# Extra top axes showing tau
# CBD
eta = pars_best_fit["CBD"][0]
tsf = tff_best_fit["CBD"] / ((1.0+eta)*epsff_best_fit)
ax0t = ax[0].twiny()
ax0t.spines["top"].set_position(("axes", 1.15))
ax0t.set_xlim(tlim/tsf)
ax0t.spines["top"].set_color("C0")
ax0t.tick_params(axis="x", colors="C0")
ax0t.text(tlim[-1]/tsf*1.03, 1.15*2200, "CBD", va="center")

# GCD
ax0t1 = ax[0].twiny()
ax0t1.spines["top"].set_position(("axes", 1.4))
ax0t1.set_frame_on(True)
ax0t1.patch.set_visible(False)
for sp in ax0t1.spines.values():
    sp.set_visible(False)
ax0t1.spines["top"].set_visible(True)
eta = pars_best_fit["GCD"][0]
tsf = tff_best_fit["GCD"] / ((1.0+eta)*epsff_best_fit)
ax0t1.set_xlim(tlim/tsf)
ax0t1.set_xlabel(r'$\tau=t/t_\mathrm{sf}$')
ax0t1.spines["top"].set_color("C1")
ax0t1.tick_params(axis="x", colors="C1")
ax0t1.text(tlim[-1]/tsf*1.03, 1.4*2200, "GCD", va="center")

# Extra right axis showing density
rholim = 3*np.pi/(32*G*(tfflim*Myr)**2)
nHlim = rholim/(1.4*mH)
ax3t = ax[3].twinx()
ax3t.set_ylim(nHlim)
ax3t.set_yscale('log')
ax3t.set_ylabel(r'$n_\mathrm{H}$ [cm$^{-3}]$')

plt.subplots_adjust(left=0.18, right=0.82, top=0.85, bottom=0.08,
                    hspace=0.1)

# Save
plt.savefig('best_fit.pdf')
