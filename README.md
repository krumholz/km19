# Data compilations and figure-generating scripts for "How Do Bound Star Clusters Form?" #

This repository contains all of the data compilations, software, and saved MCMC chains neede do generate the figures in "How Do Bound Star Clusters Form?" by Krumholz & McKee (2019, submitted to MNRAS).

### Contents and layout ###

This repository contains a main directory and two sub-directories. The main directory contains the scripts that process the data and generate the figures. Each script may be run from the command line simply as ``python script.py``. The scripts are:

* ``age_analysis.py`` -- performs the MCMC analysis of the stellar age distributions in NGC 6530 and the ONC; produces figures showing the posterior distributions and the plots of model age distributions comapred to observed ones; also writes out a table of the results in latex format
* ``combined_constraints.py`` -- produces the plots that show the overlap between the posteriors derived from the analysis of stellar ages and the posteriors derived from the analysis of epsilon_ff in the ATLASGAL sample
* ``epsff_analysis.py`` -- performs the MCMC analysis of the ATLASGAL data set; produces figures showing the posterior distributions and the plots of model epsilon_ff distributions comapred to observed ones; also writes out a table of the results in latex format
* ``example_models.py`` -- produces a figure showing example stellar mass, gas mass, and star formation histories of the various models
* ``npdfplot.py`` -- this module contains a plotting routine used by the other scripts; it is not intended to be run directly
* ``utils.py`` -- this module contains functions used by all the other scripts, for example definitions of the various models; it is not intended to be run directly

The two subdirectories are ``data`` and ``outputs``. The ``data`` directory contains all the data files required by the scripts; see the paper text for an explanation of the data sets and their sources. The ``outputs`` directory contains the computed MCMC chains. By default the scripts will read these in if they are available, rather than repeating the MCMC analysis again. If you want to re-run the MCMC for some reason, just delete or rename the .npz files in this directory.

### Dependencies ###

The scripts are compatible with both python 2 and 3. They make use of the following python packages:

* [numpy](http://www.numpy.org/)
* [scipy](https://www.scipy.org/)
* [astropy](http://www.astropy.org/)
* [matplotlib](https://matplotlib.org/)
* [emcee](https://emcee.readthedocs.io/en/stable/), Daniel Foreman-Mackey's MCMC package

### License ###

This repository is distributed under the terms of the GPL v3. A copy of the license is included in the repository.

### Contact information ###

Please contact Mark Krumholz, mark.krumholz@anu.edu.au, with any questions.