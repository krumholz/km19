"""
This module plots the combined constraints from the age and epsilon_ff
analyses. Both age_analysis.py and epsff_analysis.py must be run first.
"""

# Libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import gamma
from npdfplot import npdfplot
import os.path as osp

#############################################################

# Global parameters
models = ["ST", "CBp0", "CB", "CBD", "GC", "GCD", "IE"]

#############################################################

# Here we read in the MCMC chains from the age analysis, and calculate
# derived qauntites; we use the fiducial error case

cluster_tff =  { "NGC6530" : 0.5,
                 "ONC"     : 0.6  }
nburn_age = 200
mcmc_age = {}
for mod in models:
    mcmc_age[mod] = { }
    for cl in cluster_tff.keys():

        # Read data
        fname = osp.join("outputs",
                         "mcmc_age_{:s}_{:s}.npz".format(
                             cl, mod))
        with np.load(fname) as data:
            mcmc_age[mod][cl] = {
                "chains" : data["chains"],
                "lnL"    : data["lnL"] }

        # Extract samples
        chains = mcmc_age[mod][cl]["chains"]
        ndim = chains.shape[-1]
        samples = chains[:,nburn_age:,:].reshape(-1,ndim)
        nsamp = samples.shape[0]

        # Generate a random eta drawn from a uniform distribution over
        # our prior, since eta is unconstrained by the SF history
        eta = 10.**(np.random.rand(nsamp)*3 - 2.0)
        mcmc_age[mod][cl]["eta"] = eta

        # Compute epsilon_ff and model-dependent dimensionless
        # parameters
        t_obs = 10.**samples[:,0]
        t_sf = 10.**samples[:,1]
        t_ff = cluster_tff[cl]
        if mod != "GC" and mod != "GCD" and mod != "IE":
            eff = t_ff / ((1.0+eta)*t_sf)
        elif mod == "GC" or mod == "GCD":
            # For GC/GCD, we assign a random xi with a uniform
            # distribution over our prior for the cases where t_coll <
            # t_obs, and calculate xi from t_coll and the free-fall
            # time in the star cluster for t_coll > t_obs
            t_coll = 10.**samples[:,2]
            xi = 10.**(np.random.rand(nsamp)*2-1)
            idx = t_obs < t_coll
            xi[idx] = 2.0 * t_ff / (t_coll[idx] - t_obs[idx])
            eff = t_coll * xi/2. / ((1.0+eta)*t_sf)
            mcmc_age[mod][cl]["xi"] = xi
        elif mod == "IE":
            delta = samples[:,2]
            chi = t_sf / t_ff
            eff0 = t_ff / ((1.0+eta)*t_sf)
            eff = delta/((1+delta)**(1/(1+delta))) * \
                  gamma(delta/(1+delta)) * \
                  chi**(delta/(1+delta)) * eff0
            mcmc_age[mod][cl]["chi"] = chi
        mcmc_age[mod][cl]["eff"] = eff

        # For GC and GCD, also constrain f_GCD and epsff_eff
        if mod == "GC" or mod == "GCD":
            tau_c = t_coll / t_sf
            if mod == "GCD":
                t_fb = 10.**samples[:,3]
                phi_d = 10.**samples[:,4]
                x_fb = t_fb / t_coll
                f_gcd = ((1.0+tau_c*phi_d) *
                         (1.0 - (phi_d-1.0)/phi_d * (1.0-x_fb)**tau_c)) / \
                         (1.0 + tau_c*phi_d *
                          (1.0 - (phi_d-1.0)/phi_d *
                           (1.0-x_fb)**(tau_c+1)))
            else:
                f_gcd = 1.0
            mcmc_age[mod][cl]["f_gcd"] = f_gcd
            mcmc_age[mod][cl]["epsff_eff"] = eff * (1.0+tau_c) / tau_c


#############################################################

# Here we read in the MCMC chains from the ATLASGAL analysis, and
# calculate derived quantites; we use the fiducial M_gas/M_* case

mratio = 1.0
tff_atlasgal = 0.3
nburn_epsff = 200
mcmc_epsff = {}
for mod in models:

    # Read data
    fname = osp.join("outputs",
                     "mcmc_epsff_{:s}_mgms{:04.1f}.npz".format(
                         mod, mratio))
    with np.load(fname) as data:
        mcmc_epsff[mod] = {
            "chains" : data["chains"],
            "lnL" : data["lnL"] }

    # Extract samples
    chains = mcmc_epsff[mod]["chains"]
    ndim = chains.shape[-1]
    samples = chains[:,nburn_epsff:,:].reshape(-1,ndim)
    nsamp = samples.shape[0]
    
    # Derive epsilon_ff
    eta = 10.**samples[:,1]
    t_sf = 10.**samples[:,2]
    if mod != "GC" and mod != "GCD" and mod != "IE":
        eff = tff_atlasgal / ((1.0+eta)*t_sf)
    elif mod == "GC" or mod == "GCD":
        t_coll = 10.**samples[:,3]
        xi = 10.**samples[:,4]
        eff = t_coll * xi/2. / ((1.0+eta)*t_sf)
    elif mod == "IE":
        delta = samples[:,3]
        chi = t_sf / tff_atlasgal
        eff0 = tff_atlasgal / ((1.0+eta)*t_sf)
        eff = delta/((1+delta)**(1/(1+delta))) * \
              gamma(delta/(1+delta)) * \
              chi**(delta/(1+delta)) * eff0
        mcmc_epsff[mod]["chi"] = chi
    mcmc_epsff[mod]["eff" ] = eff

    # For GC and GCD, also constrain f_GCD and effective eps_ff
    if mod == "GC" or mod == "GCD":
        tau_c = t_coll / t_sf
        if mod == "GCD":
            t_fb = 10.**samples[:,5]
            phi_d = 10.**samples[:,6]
            x_fb = t_fb / t_coll
            f_gcd = ((1.0+tau_c*phi_d) *
                     (1.0 - (phi_d-1.0)/phi_d * (1.0-x_fb)**tau_c)) / \
                     (1.0 + tau_c*phi_d *
                      (1.0 - (phi_d-1.0)/phi_d *
                       (1.0-x_fb)**(tau_c+1)))
        else:
            f_gcd = 1.0
        mcmc_epsff[mod]["f_gcd"] = f_gcd
        mcmc_epsff[mod]["epsff_eff"] = eff * (1.0+tau_c) / tau_c


#############################################################

# Make plots of joint constraints on dimensionless parameters; use NGC
# 6530 + ATLASGAL

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
figsize = (5,5)

cl = 'NGC6530'
for mod in models:
        
    # Grab dimensionless parameters from age analysis
    logeff = np.log10(mcmc_age[mod][cl]["eff"])
    logeta = np.log10(mcmc_age[mod][cl]["eta"])
    samples = np.hstack((logeff.reshape(logeff.size,1),
                         logeta.reshape(logeta.size,1)))
    labels = [ r"$\log\,\epsilon_{\mathrm{ff}}$",
               r"$\log\,\eta$" ]
    lolim = [-3, -2]
    hilim = [0, 1]
    logt_sf = mcmc_age[mod][cl]["chains"][:,nburn_age:,1].reshape(-1,1)
    if mod == "CB" or mod == "CBD" or mod == "CBp0":
        logt_acc = mcmc_age[mod][cl]["chains"][:,nburn_age:,2].reshape(-1,1)
        logtau_acc = logt_acc - logt_sf
        samples = np.hstack((samples, logtau_acc))
        labels.append(r"$\log\,\tau_{\mathrm{acc}}$")
        lolim.append(-2)
        hilim.append(2)
    if mod == "CBD":
        logphi_d = mcmc_age[mod][cl]["chains"][:,nburn_age:,3].reshape(-1,1)
        samples = np.hstack((samples, logphi_d))
        labels.append(r"$\log\,\phi_{\mathrm{d}}$")
        lolim.append(0)
        hilim.append(2)
    if mod == "GC" or mod == "GCD":
        logt_c = mcmc_age[mod][cl]["chains"][:,nburn_age:,2].reshape(-1,1)
        logtau_c = logt_c - logt_sf
        samples = np.hstack((samples, logtau_c))
        labels.append(r"$\log\,\tau_{\mathrm{coll}}$")
        lolim.append(-2)
        hilim.append(0)
        logxi = np.log10(mcmc_age[mod][cl]["xi"]).reshape(-1,1)
        samples = np.hstack((samples, logxi))
        labels.append(r"$\log\,\xi$")
        lolim.append(-1)
        hilim.append(1)
    if mod == "GCD":
        logt_fb = mcmc_age[mod][cl]["chains"][:,nburn_age:,3].reshape(-1,1)
        logx_fb = logt_fb - logt_c
        samples = np.hstack((samples, logx_fb))
        labels.append(r"$\log\,x_{\mathrm{fb}}$")
        lolim.append(-2)
        hilim.append(0)
        logphi_d = mcmc_age[mod][cl]["chains"][:,nburn_age:,4].reshape(-1,1)
        samples = np.hstack((samples, logphi_d))
        labels.append(r"$\log\,\phi_{\mathrm{d}}$")
        lolim.append(0)
        hilim.append(2)
    if mod == "IE":
        delta = mcmc_age[mod][cl]["chains"][:,nburn_age:,2].reshape(-1,1)
        samples = np.hstack((samples, delta))
        labels.append(r"$\log\,\delta$")
        lolim.append(0)
        hilim.append(3)
        logchi = np.log10(mcmc_age[mod][cl]["chi"]).reshape(-1,1)
        samples = np.hstack((samples, logchi))
        labels.append(r"$\log\,\chi$")
        lolim.append(0)
        hilim.append(6)

    # Make corner plot based on age analysis
    gs, img1 = npdfplot(samples, figsize=figsize,
                        labels=labels, lolim=lolim, hilim=hilim,
                        show_as_contour=True, hist_fc='r', hist_ec='none',
                        hist_alpha=0.5, colorbar=False,
                        histlim=[[0.01,2]]*samples.shape[0],
                        fignum=1, thresh=1.0e-2,
                        contour_alpha=0.5, thresh_line=False, scat_alpha=0.0,
                        nbin=30, n_contour_lev=6, return_gs=True,
                        return_img=True)

    # Adjust spacing
    if mod != "ST":
        plt.subplots_adjust(hspace=0.25, wspace=0.17, right=0.85, top=0.95,
                            left=0.12, bottom=0.12)
    else:
        plt.subplots_adjust(hspace=0.25, wspace=0.3, right=0.85, top=0.95,
                            left=0.12, bottom=0.12)
        
    # Grab dimensionless parameters from eps_ff analysis
    logeff = np.log10(mcmc_epsff[mod]["eff"])
    logeta = mcmc_epsff[mod]["chains"][:,nburn_epsff:,1].reshape(-1,1)
    samples = np.hstack((logeff.reshape(logeff.size,1),
                         logeta.reshape(logeta.size,1)))
    logt_sf = mcmc_epsff[mod]["chains"][:,nburn_epsff:,2].reshape(-1,1)
    if mod == "CB" or mod == "CBD" or mod == "CBp0":
        logt_acc = mcmc_epsff[mod]["chains"][:,nburn_epsff:,3].reshape(-1,1)
        logtau_acc = logt_acc - logt_sf
        samples = np.hstack((samples, logtau_acc))
    if mod == "CBD":
        logphi_d = mcmc_epsff[mod]["chains"][:,nburn_epsff:,4].reshape(-1,1)
        samples = np.hstack((samples, logphi_d))
    if mod == "GC" or mod == "GCD":
        logt_c = mcmc_epsff[mod]["chains"][:,nburn_epsff:,3].reshape(-1,1)
        logtau_c = logt_c - logt_sf
        samples = np.hstack((samples, logtau_c))
        logxi = mcmc_epsff[mod]["chains"][:,nburn_epsff:,4].reshape(-1,1)
        samples = np.hstack((samples, logxi))
    if mod == "GCD":
        logt_fb = mcmc_epsff[mod]["chains"][:,nburn_epsff:,5].reshape(-1,1)
        logx_fb = logt_fb - logt_c
        samples = np.hstack((samples, logx_fb))
        logphi_d = mcmc_epsff[mod]["chains"][:,nburn_epsff:,6].reshape(-1,1)
        samples = np.hstack((samples, logphi_d))        
    if mod == "IE":
        delta = mcmc_epsff[mod]["chains"][:,nburn_epsff:,3].reshape(-1,1)
        samples = np.hstack((samples, delta))
        logchi = np.log10(mcmc_epsff[mod]["chi"]).reshape(-1,1)
        samples = np.hstack((samples, logchi))

    # Overplot posterior PDFs derived from ATLASGAL
    img2 = npdfplot(samples, labels=labels, lolim=lolim, hilim=hilim,
                    show_as_contour=True, hist_fc='C0', hist_ec='none',
                    hist_alpha=0.5, colorbar=False,
                    fignum=1, thresh=1.0e-2, gs=gs,
                    contour_alpha=0.5, thresh_line=False, scat_alpha=0.0,
                    nbin=30, n_contour_lev=6, cmap='Blues',
                    return_img=True)

    # Add color bars
    ax = plt.subplot(gs[0,samples.shape[-1]-1])
    if mod != "ST":
        cbar1 = plt.colorbar(img1, ax=ax, fraction=0.5, aspect=10,
                             label=r'$\log\,p$ (age)')
        cbar2 = plt.colorbar(img2, ax=ax, fraction=0.5, aspect=10,
                             label=r'$\log\,p$ (YSO)')
    else:
        cbar1 = plt.colorbar(img1, ax=ax, fraction=0.1, aspect=20,
                             label=r'$\log\,p$ (age)')
        cbar2 = plt.colorbar(img2, ax=ax, fraction=0.1, aspect=20,
                             label=r'$\log\,p$ (YSO)')
    cbar2.ax.yaxis.set_ticks_position('left')
    cbar2.ax.yaxis.set_label_position('left')
    ax.remove()
    
    # Save to file
    plt.savefig("combined_pdf_{:s}.pdf".format(mod))


# Now make a plot showing predicted ATLASGAL SFRs for CBD and GCD,
# following constraints from YSO ages and counts
models = ["CBD", "GC", "GCD"]
nbin = [4, 10, 10]
m_atlasgal = 1e7
plt.figure(2, figsize=(5,3.5))
plt.clf()
for m, n in zip(models, nbin):

    # Step 1: find the 16 - 84 percentile confidence interval on eps_ff
    # from the YSO counts
    lim = np.percentile(mcmc_epsff[m]["eff"], [16, 84])

    # Step 2: select the models from the age distribution that are
    # consistent with that eps_ff
    idx = np.logical_and(
        mcmc_age[m][cl]["eff"] >= lim[0],
        mcmc_age[m][cl]["eff"] <= lim[1])

    # Step 3: get epsff_eff for models satisfying these constraints
    if m == "GC" or m == "GCD":
        epsff_eff = mcmc_age[m][cl]["epsff_eff"][idx]
    else:
        epsff_eff = mcmc_age[m][cl]["eff"][idx]

    # Step 4: plot distribution of SFRs for these models
    hist, logsfr = np.histogram(
        np.log10(epsff_eff * m_atlasgal/(tff_atlasgal*1e6)),
        bins=n)
    hist = hist / np.amax(hist)  # Normalise to maximum of unity
    plt.bar(10.**logsfr[:-1], hist, 10.**logsfr[1:]-10.**logsfr[:-1],
            align='edge', alpha=0.5, label=m)

# Add plot for total galactic SFR
plt.plot(2.0*np.ones(2),
         [0, 1.3], 'k--')
plt.arrow(2.0, 1.0, -0.9, 0, ec='k', fc='k',
          head_length=0.08, head_width=0.04)
plt.text(2.3, 1, 'Galactic SFR',
         ha='left', va='center', rotation=90)

# Adjust limits, add labels and legend
plt.xlim(10.**-0.5, 10.**2.5)
plt.xscale('log')
plt.ylim([0, 1.3])
plt.legend(ncol=2)
plt.xlabel(r'ATLASGAL SFR [$M_\odot$ yr$^{-1}$]')
plt.ylabel('PDF (arb. units)')
plt.subplots_adjust(bottom=0.15, top=0.95, right=0.95)
plt.savefig('SFR_pdf.pdf')
