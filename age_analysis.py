"""
This module carries out the analysis of the cluster age distributions
"""

# Libraries
import numpy as np
import multiprocessing
import matplotlib.pyplot as plt
import astropy.io.ascii as asc
import astropy.io.fits as fits
import astropy.units as u
import os.path as osp
import emcee
from npdfplot import npdfplot
from utils import models, age_dist
from scipy.special import gamma


#############################################################

# Import data

# Data directory
datadir = "data"

# Read Kounkel+ 2018 catalog of positions and ages
kdata = asc.read(osp.join(datadir, 'kounkel2018.txt'))

# Sky position and distance to ONC
ra_th1c  = 83.81859895833333   # RA of theta 1 Ori C, in degrees
dec_th1c = -5.390235722222223  # DEC of theta 1 Ori C, in degrees
dist_onc = 0.389*u.kpc  # Distance to ONC, from Kounkel+ 2018
r_onc_phys = 1*u.pc
r_onc = (r_onc_phys/dist_onc).to('') * 180/np.pi

# Get ages of ONC stars
age = np.copy(kdata['age_hr'])
age_hr = np.ones(len(age), dtype=bool)
idx = np.logical_not(np.nan_to_num(age) > 1e-2)
age[idx] = kdata['age_cmd'][idx]
age_hr[idx] = False
idx = np.nan_to_num(age) > 0
ra = kdata['ra'][idx]
dec = kdata['dec'][idx]
group = kdata['cloud'][idx]
age = age[idx]
age_hr = age_hr[idx]
idx = np.logical_and.reduce(
    (group == 1,
     np.sqrt((ra - ra_th1c)**2 + (dec - dec_th1c)**2) < r_onc,
     age_hr))
age_onc = age[idx]
t_onc = np.sort(age_onc)

# Free-fall time in the ONC in Myr (from da Rio+ 2014)
t_ff_onc = 0.6

# Read NGC 6530 data (from Prisinzano+ 2019); free-fall time is
# estimated as described in the paper
pdata=fits.open(osp.join(datadir, 'prisinzano19.fits'))
t_ngc6530 = 10.**(pdata[1].data['t']-6)
t_ngc6530 = t_ngc6530[np.isfinite(t_ngc6530)]
t_ngc6530 = np.sort(t_ngc6530)
t_ff_ngc6530 = 0.5
pdata.close()

# Build structure to store imported data
clusters = { }
clusters["ONC"] = { "t_star" : t_onc,
                    "t_ff"  : t_ff_onc }
clusters["NGC6530"] = { "t_star" : t_ngc6530,
                        "t_ff"  : t_ff_ngc6530 }

#############################################################

# Set up list of models that we will be evaluating, with chosen
# parameters and limits for each
model_names = [ "ST", "CBp0", "CB", "CBD", "GC", "GCD", "IE"]

# Parameters we fix; these do not affect the shape of the age
# distribution, but some are important to enforcing limits on
# priors
fixed_pars = {
    "ST"  :  { "eta" : 1.0 },
    "CB"  :  { "eta" : 1.0, "p" : 3 },
    "CBp0" : { "eta" : 1.0, "p" : 0 },
    "CBD" :  { "eta" : 1.0, "p" : 3 },
    "GC" :   { "eta" : 1.0, "xi" : 1.0 },
    "GCD" :  { "eta" : 1.0, "xi" : 1.0 },
    "IE"  :  { "eta" : 1.0 }
}

# Limits we will enforce on priors for particular models
par_limits = {
    "ST"   : {},
    "CB"   : { "t_acc_lim"  : [ 0.01, 100.] },
    "CBp0" : { "t_acc_lim"  : [ 0.01, 100.] },
    "CBD"  : { "t_acc_lim"  : [ 0.01, 100.], 
               "phi_d_lim"  : [ 1.0, 100.] },
    "GC"   : { "t_coll_lim" : [ 0.01, 100.],
               "xi_lim"     : [ 0.1, 10.] },
    "GCD"  : { "t_coll_lim" : [ 0.01, 100.],
               "xi_lim"     : [ 0.1, 10.],
               "t_fb_lim"   : [ 0.01, 100.],
               "phi_d_lim"  : [ 1.0, 100.] },
    "IE"   : { "delta_lim"  : [ 0.0, 3.0] }
}

# Starting values for the MCMC; these don't really matter, they just
# need to be inside the range of allowable priors
par_start = {
    "ST"   : [ ],               # No other parameters
    "CB"   : [ 1.0 ],           # log t_acc,
    "CBp0" : [ 1.0 ],           # log t_acc,
    "CBD"  : [ 1.0, 1.0 ],      # log t_acc, log phi_d
    "GC"   : [ 1.5 ],           # log t_coll
    "GCD"  : [ 1.5, 1.0, 1.0 ], # log t_coll, log t_fb, log phi_d
    "IE"   : [ 0.1 ]            # delta
}

# Pack model info
model_params = { }
for mname in model_names:
    model_params[mname] = { "fixed_pars" : fixed_pars[mname],
                            "par_limits" : par_limits[mname],
                            "par_start"  : par_start[mname]
                          }

# Specify the error model
sigma = 0.13       # Dispersion
bias = -0.05       # Bias

#############################################################

# Set up the function that returns the log likelihood; this is what
# emcee will call

# Limits and priors we use for all models
eff_lim = [1.0e-4, 1.0]
t_obs_lim = [ 0.01, 100. ]
eps_lim = 0.05

def lnL_age_dist(theta, t_star_obs, t_ff, name, sigma, bias,
                 par_limits, fixed_pars):

    # Unpack input parameters that are the same for all models
    t_obs = 10.**theta[0]
    t_sf = 10.**theta[1]
    
    # Enforce limits
    if t_obs < t_obs_lim[0] or t_obs > t_obs_lim[1]: return -np.inf

    # Unpack the model-dependent input parameters, if any; enforce
    # limits, and generate parameter list to be passed to age_dist
    # function
    eta = fixed_pars["eta"]
    if name == "ST":
        eff = t_ff / ((1.0+eta) * t_sf)
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        pars = (eta,) # eta
        modname = "ST"
    elif name == "CB" or name == "CBp0":
        eff = t_ff / ((1.0+eta) * t_sf)
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        t_acc = 10.**theta[2]
        if t_acc < par_limits["t_acc_lim"][0] or \
           t_acc > t_obs:
            return -np.inf
        if t_acc < t_ff: return -np.inf
        pars = (eta,
                fixed_pars["p"],
                t_acc/t_sf)  # eta, p, tau_acc
        modname = "CB"
    elif name == "CBD":
        eff = t_ff / ((1.0+eta) * t_sf)
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        t_acc = 10.**theta[2]
        phi_d = 10.**theta[3]
        if t_acc < par_limits["t_acc_lim"][0] or \
           t_acc > t_obs or \
           phi_d < par_limits["phi_d_lim"][0] or \
           phi_d > par_limits["phi_d_lim"][1]: 
            return -np.inf
        if t_acc < t_ff: return -np.inf
        pars = (eta,
                fixed_pars["p"],
                t_acc/t_sf,
                (1.0+eta)*phi_d - 1.0)  # eta, p, tau_acc, eta_d
        modname = "CBD"
    elif name == "GC":
        t_coll = 10.**theta[2]
        if t_coll < par_limits["t_coll_lim"][0] or \
           t_coll > par_limits["t_coll_lim"][1]: 
            return -np.inf
        # GC model requires some care, because there are two branches: either
        # the cluster is pre-collapse, in which case xi is not free because
        # it is fixed by the requirement that t_ff = observed t_ff, or we say
        # it is post-collapse, in which case xi is free and we have to fix it
        # to unity. In the former case, we enforce xi > xi_min
        if t_obs >= t_coll:
            xi = fixed_pars["xi"]
        else:
            xi = 2.0 * t_ff / (t_coll - t_obs)
            if xi < par_limits["xi_lim"][0] or \
               xi > par_limits["xi_lim"][1]:
                return -np.inf
        eff = 0.5 * t_coll * xi / ((1.0+eta)*t_sf)
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        pars = (eta, 
                t_coll/t_sf) # eta, tau_coll
        modname = "GC"
    elif name == "GCD":
        # GCD model: see notes on GC model for some explanations
        t_coll = 10.**theta[2]
        t_fb = 10.**theta[3]
        phi_d = 10.**theta[4]
        if t_coll < par_limits["t_coll_lim"][0] or \
           t_coll > par_limits["t_coll_lim"][1] or \
           t_fb < par_limits["t_fb_lim"][0] or \
           t_fb > par_limits["t_fb_lim"][1] or \
           t_fb >= t_coll or \
           phi_d < par_limits["phi_d_lim"][0] or \
           phi_d > par_limits["phi_d_lim"][1]: 
            return -np.inf
        if t_obs >= t_coll:
            xi = fixed_pars["xi"]
        else:
            xi = 2.0 * t_ff / (t_coll - t_obs)
            if xi < par_limits["xi_lim"][0] or \
               xi > par_limits["xi_lim"][1]:
                return -np.inf
        eff = 0.5 * t_coll * xi / ((1.0+eta)*t_sf)
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        pars = (eta, 
                t_coll/t_sf,
                t_fb/t_sf,
                phi_d) # eta, tau_coll, tau_fb, phi_d
        modname = "GCD"
    elif name == "IE":
        delta = theta[2]
        if delta < par_limits["delta_lim"][0] or \
           delta > par_limits["delta_lim"][1]:
            return -np.inf
        chi = t_sf/t_ff
        eff0 = t_ff / ((1.0+eta) * t_sf)
        eff = delta/((1.+delta)**(1./(1.+delta))) * \
              gamma(delta/(1.+delta)) * \
              chi**(delta/(1.+delta)) * eff0
        if eff < eff_lim[0] or eff > eff_lim[1]: return -np.inf
        pars = (eta, 
                delta, 
                chi)  # eta, delta, chi
        modname = "IE"
        
    # Evaulate the PDF of ages for all observed stars
    tau_obs = t_obs / t_sf
    tau_star = t_star_obs / t_sf
    pdf = age_dist(tau_obs, tau_star, pars, modname, sigma, bias, log=True)
    
    # Compute likelihood
    lnL = np.sum(np.log(pdf))
    
    # Apply prior on epsilon_*
    eps = models(tau_obs, pars, modname, "s")
    if eps < eps_lim:
        lnL = lnL - (eps_lim/eps)**2
    
    # Return
    return lnL


#############################################################

# Here is the main part of the program, where we loop over all
# clusters and all models

# Fixed parameters of the MCMC
nwalkers = 100
nsteps = 1000
nburn = 200
log_t_obs_init = 1.5
log_t_sf_init = 1.0

# Loop over clusters
for clname in clusters.keys():
    cl = clusters[clname]

    # Loop over models
    cl["mcmc"] = {}
    for modname in model_params.keys():
        mod = model_params[modname]

        # See if we already have the MCMC chain and log likelihood
        # for this case stored; if so, just read it and skip
        fname = "mcmc_age_{:s}_{:s}.npz".format(
            clname, modname)
        try:
            with np.load(osp.join("outputs", fname)) as data:
                cl["mcmc"][modname] = {
                    "chains" : data["chains"],
                    "lnL"    : data["lnL"]
                }
            print("Read data for cluster {:s}, model {:s} "
                  "from file {:s}".format(
                      clname, modname, fname))
            continue
        except IOError:
            pass

        # Print status
        print(("Fitting cluster {:s}, model {:s}").format(
            clname, modname))

        # Set the number of dimensions and initial positions for
        # this model
        ndim = 2 + len(mod["par_start"])
        initpos = [ np.array(
            [log_t_obs_init, log_t_sf_init] + mod["par_start"])  +
                    0.02*np.random.randn(ndim)
                    for i in range(nwalkers) ]

        # Set up the MCMC sampler
        sampler = emcee.EnsembleSampler(
            nwalkers, ndim, lnL_age_dist,
            threads=multiprocessing.cpu_count(),
            args=(cl["t_star"], cl["t_ff"], modname,
                  sigma, bias,
                  mod["par_limits"], mod["fixed_pars"]))

        # Run the MCMC
        d = sampler.run_mcmc(initpos, nsteps)

        # Store results
        cl["mcmc"][modname] = {
            "chains" : sampler.chain,
            "lnL"    : sampler.lnprobability
        }

        # Write to file
        np.savez(osp.join("outputs", fname),
                 chains=sampler.chain,
                 lnL=sampler.lnprobability)

#############################################################

# Analysis section: here we compute various derived quantities and
# their statistical properties

# Loop over clusters and models
for clname in clusters.keys():
    cl = clusters[clname]
    for modname in model_params.keys():
        mod = model_params[modname]
            
        # Extract samples
        mcmc = cl["mcmc"][modname]
        chains = cl["mcmc"][modname]["chains"]
        ndim = chains.shape[-1]
        samples = chains[:,nburn:,:].reshape(-1,ndim)

        # Get percentiles on marginal posteriors of all parameters
        mcmc["pct"] = []
        for n in range(ndim):
            mcmc["pct"].append(
                np.percentile(samples[:,n], [16,50,84]))

        # Get epsilon_ff (and xi for model GC)
        eta = mod["fixed_pars"]["eta"]
        t_obs = 10.**samples[:,0]
        t_sf = 10.**samples[:,1]
        t_ff = cl["t_ff"]
        if modname != "GC" and modname != "GCD" and modname != "IE":
            eff = t_ff / ((1.0+eta)*t_sf)
        elif modname == "GC" or modname == "GCD":
            t_coll = 10.**samples[:,2]
            if modname == "GCD":
                t_fb = 10.**samples[:,3]
                phi_d = 10.**samples[:,4]
            xi = np.ones(t_coll.shape) * mod["fixed_pars"]["xi"]
            idx = t_obs < t_coll
            xi[idx] = 2.0 * t_ff / (t_coll[idx] - t_obs[idx])
            eff = t_coll * xi/2. / ((1.0+eta)*t_sf)
            mcmc["xi"] = xi
            mcmc["xi_pct"] = np.percentile(xi, [16,50,84])
        elif modname == "IE":
            delta = samples[:,2]
            chi = t_sf / t_ff
            eff0 = t_ff / ((1.0+eta)*t_sf)
            eff = delta/((1+delta)**(1/(1+delta))) * \
                  gamma(delta/(1+delta)) * \
                  chi**(delta/(1+delta)) * eff0
        mcmc["eff"] = eff
        mcmc["eff_pct"] = np.percentile(eff, [16,50,84])

        # Get M_* / M_tot and M_g / (M_* + M_g) in final state
        if modname == "ST":
            pars = (eta,)
            mname = "ST"
        elif modname == "CB" or modname == "CBp0":
            t_acc = 10.**samples[:,2]
            pars = (eta, mod["fixed_pars"]["p"], t_acc/t_sf)
            mname = "CB"
        elif modname == "CBD":
            t_acc = 10.**samples[:,2]
            phi_d = 10.**samples[:,3]
            pars = (eta, mod["fixed_pars"]["p"], t_acc/t_sf,
                    phi_d)
            mname = "CBD"
        elif modname == "GC":
            pars = (eta, t_coll/t_sf)
            mname = "GC"
        elif modname == "GCD":
            pars = (eta, t_coll/t_sf, t_fb/t_sf, phi_d)
            mname = "GCD"
        elif modname == "IE":
            pars = (eta, delta, chi)
            mname = "IE"
        tau = t_obs / t_sf
        mg, ms, sfr = models(tau, pars, mname, 'all')
        mcmc['fg'] = mg/(ms+mg)
        mcmc['fg_pct'] = np.percentile(mg/(ms+mg), [16,50,84])
        mcmc['epss'] = ms
        mcmc['epss_pct'] = np.percentile(ms, [16,50,84])

#############################################################

# Reporting section: here we print out a latex-formatted summary table
# of the results

latex_names = ["ST", 
               "CB, $p=0$", 
               "CB, $p=3$", 
               "CBD, $p=3$", 
               "GC",
               "GCD",
               "IE"]
print("")
print("")
print("*************************************************************")
print("")
print("")

print("\\begin{tabular}{c@{\\qquad\\qquad}ccc@{\\qquad\\qquad}ccc}")
print("\\hline\\hline")
print("Model & \multicolumn{3}{c}{Fit parameters} & "
      "\multicolumn{3}{c}{Derived parameters} \\\\")
print("      &  $\\log\\tobs$  &  $\\log\\tsf$  &"
      "   Other  &  $\\log\\eff$  &   "
      "$f_{\\rm g,clust}$  & $\\log\\epsilon_{\\rm *,clust}$  \\\\")
print("      &  [Myr]        &  [Myr]  \\\\")
for clname in clusters.keys():
    cl = clusters[clname]
    print("\\hline")
    print("\\multicolumn{{7}}{{c}}{{{:s}}} \\\\ \\hline".format(clname))
    for n, k in zip(latex_names, model_names):
        mcmc = cl["mcmc"][k]
        outstr = ""
        if k == "GCD":
            outstr += "\\multirow{2}{*}{"
        outstr += "{:s}  ".format(n)
        if k == "GCD":
            outstr += "}"
        for i in range(2):
            outstr += "&   "
            if k == "GCD":
                outstr += "\\multirow{2}{*}{"
            outstr += "${:4.2f}_{{{:+5.2f}}}^{{{:+5.2f}}}$  ".format(
                mcmc["pct"][i][1],
                mcmc["pct"][i][0]-mcmc["pct"][i][1],
                mcmc["pct"][i][2]-mcmc["pct"][i][1]) 
            if k == "GCD":
                outstr += "}"
        if k == "ST":
            outstr += "&   --  "
        elif k == "CB" or k == "CBp0":
            outstr += ("&    $\\log\\ta = {:4.2f}_{{{:+5.2f}}}"
                       "^{{{:+5.2f}}}$   ").format(
                           mcmc["pct"][2][1],
                           mcmc["pct"][2][0]-mcmc["pct"][2][1],
                           mcmc["pct"][2][2]-mcmc["pct"][2][1])
        elif k == "CBD":
            outstr += ("&    $\\log\\ta = {:4.2f}_{{{:+5.2f}}}"
                       "^{{{:+5.2f}}}$, ").format(
                           mcmc["pct"][2][1],
                           mcmc["pct"][2][0]-mcmc["pct"][2][1],
                           mcmc["pct"][2][2]-mcmc["pct"][2][1])
            outstr += ("$\\log\\phid = {:4.2f}_"
                       "{{{:+5.2f}}}^{{{:+5.2f}}}$   ").format(
                           mcmc["pct"][3][1],
                           mcmc["pct"][3][0]-mcmc["pct"][3][1],
                           mcmc["pct"][3][2]-mcmc["pct"][3][1])
        elif k == "GC":
            outstr += ("&    $\\log\\tc = {:4.2f}_{{{:+5.2f}}}"
                       "^{{{:+5.2f}}}$, ").format(
                           mcmc["pct"][2][1],
                           mcmc["pct"][2][0]-mcmc["pct"][2][1],
                           mcmc["pct"][2][2]-mcmc["pct"][2][1])
            outstr += ("$\\xi = {:4.2f}_{{{:+5.2f}}}"
                       "^{{{:+5.2f}}}$   ").format(
                           mcmc["xi_pct"][1],
                           mcmc["xi_pct"][0]-mcmc["xi_pct"][1],
                           mcmc["xi_pct"][2]-mcmc["xi_pct"][1])
        elif k == "GCD":
            outstr += ("&    $\\log\\tc = {:4.2f}_{{{:+5.2f}}}"
                       "^{{{:+5.2f}}}$, ").format(
                           mcmc["pct"][2][1],
                           mcmc["pct"][2][0]-mcmc["pct"][2][1],
                           mcmc["pct"][2][2]-mcmc["pct"][2][1])
            outstr += ("$\\log\\tfb = {:4.2f}_{{{:+5.2f}}}"
                       "^{{{:+5.2f}}}$, ").format(
                           mcmc["pct"][3][1],
                           mcmc["pct"][3][0]-mcmc["pct"][3][1],
                           mcmc["pct"][3][2]-mcmc["pct"][3][1])
        elif k == "IE":
            outstr += ("&    $\\delta = {:4.2f}_{{{:+5.2f}}}"
                       "^{{{:+5.2f}}}$   ").format(
                           mcmc["pct"][2][1],
                           mcmc["pct"][2][0]-mcmc["pct"][2][1],
                           mcmc["pct"][2][2]-mcmc["pct"][2][1])
        outstr += "&    "
        if k == "GCD":
            outstr += "\\multirow{2}{*}{"
        outstr += "${:4.2f}_{{{:+5.2f}}}^{{{:+5.2f}}}$   ".format(
            np.log10(mcmc["eff_pct"][1]),
            np.log10(mcmc["eff_pct"][0]/mcmc["eff_pct"][1]),
            np.log10(mcmc["eff_pct"][2]/mcmc["eff_pct"][1]))
        if k == "GCD":
            outstr += "}"
        outstr += "&   "
        if k == "GCD":
            outstr += "\\multirow{2}{*}{"
        outstr += "${:4.2f}_{{{:+5.2f}}}^{{{:+5.2f}}}$   ".format(
            mcmc["fg_pct"][1],
            mcmc["fg_pct"][0]-mcmc["fg_pct"][1],
            mcmc["fg_pct"][2]-mcmc["fg_pct"][1])
        if k == "GCD":
            outstr += "}"
        outstr += "&   "
        if k == "GCD":
            outstr += "\\multirow{2}{*}{"
        outstr += "${:4.2f}_{{{:+5.2f}}}^{{{:+5.2f}}}$   ".format(
            np.log10(mcmc["epss_pct"][1]),
            np.log10(mcmc["epss_pct"][0]/mcmc["epss_pct"][1]),
            np.log10(mcmc["epss_pct"][2]/mcmc["epss_pct"][1]))
        if k == "GCD":
            outstr += "}"
        if k == "GCD":
            outstr += "\\\\ [0.5ex]"
            outstr += "& & & "
            outstr += ("$\\log\\phid = {:4.2f}_{{{:+5.2f}}}"
                       "^{{{:+5.2f}}}$, ").format(
                           mcmc["pct"][4][1],
                           mcmc["pct"][4][0]-mcmc["pct"][4][1],
                           mcmc["pct"][4][2]-mcmc["pct"][4][1])
            outstr += ("$\\xi = {:4.2f}_{{{:+5.2f}}}"
                       "^{{{:+5.2f}}}$   ").format(
                           mcmc["xi_pct"][1],
                           mcmc["xi_pct"][0]-mcmc["xi_pct"][1],
                           mcmc["xi_pct"][2]-mcmc["xi_pct"][1])
        outstr += "\\\\ [1.5ex]"
        print(outstr)
print("\\hline\\hline")
print("\\end{tabular}")


#############################################################

# Plot observed SF history versus best fits

# Make plots of SF history using best fit parameters
plt.figure(1, figsize=(7,5))
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.clf()

# List of models to plot, and their labels
cases_to_plot = [["ST", "CBp0"], ["CB", "CBD"], ["GC", "GCD", "IE"]]
plot_labels = { "ST" : "ST", 
                "CBp0" : r"CB, $p=0$",
                "CB" : r"CB, $p=3$", 
                "CBD" : r"CBD, $p=3$",
                "GC" : "GC",
                "GCD": "GCD",
                "IE" : "IE" }

# General setup
tlim = [-1,1.5]
t = np.logspace(tlim[0], tlim[1], 500)
ylim = [0, 2]

# Loop over clusters
for j, clname in enumerate(clusters.keys()):
    cl = clusters[clname]

    # Get observed distribution
    t_star = cl["t_star"]
    t_ff = cl["t_ff"]

    # Loop over plot panels
    ctr = 0
    for i in range(len(cases_to_plot)):
    
        # Add panel
        plt.subplot(len(clusters),len(cases_to_plot),
                    i+j*len(cases_to_plot)+1)
    
        # Plot observed CDF and free-fall time
        if i == 0 and j == 1: lab = "Observed"
        else: lab = None
        t_star_plot = np.sort(t_star)
        n,bins,patches = plt.hist(np.log10(t_star_plot),
                                  density=True, color='k', alpha=0.25, 
                                  bins="auto", label=lab)
        if i == 0 and j == 1: lab = r"$(1,3.2,10)t_{\mathrm{ff}}$"
        plt.plot(np.log10([t_ff, t_ff]), ylim, 'k--', lw=1, label=lab)
        plt.plot(np.log10([10.**0.5*t_ff, 10.**0.5*t_ff]), ylim, 'k--', lw=1)
        plt.plot(np.log10([10*t_ff, 10*t_ff]), ylim, 'k--', lw=1)

        # Add line of slope unity to guide the eye
        if i==1 and j == 1:
            plt.plot([-0.9, -0.5], [0.7, 1.1], ':', color="#999999", lw=1,
                     label=r'$\dot{M}_* \propto \mathrm{const}$')
        else:
            plt.plot([-0.9, -0.5], [0.7, 1.1], ':', color="#999999", lw=1)

        # Overplot models from last MCMC iteration
        for k in cases_to_plot[i]:
            for w in range(0,nwalkers,5):

                # Extract data from this sample
                mcmc = cl["mcmc"][k]
                fixed_pars = model_params[k]["fixed_pars"]
                t_obs = 10.**mcmc["chains"][w,-1,0]
                t_sf = 10.**mcmc["chains"][w,-1,1]
                tau_obs = t_obs/t_sf
                if k == "ST":
                    pars = (eta,)
                    modname = "ST"
                elif k == "CB" or k == "CBp0":
                    t_acc = 10.**mcmc["chains"][w,-1,2]
                    pars = (fixed_pars["eta"], fixed_pars["p"],
                            t_acc/t_sf)
                    modname = "CB"
                elif k == "CBD":
                    t_acc = 10.**mcmc["chains"][w,-1,2]
                    phi_d = 10.**mcmc["chains"][w,-1,3]
                    pars = (fixed_pars["eta"], fixed_pars["p"],
                            t_acc/t_sf, 
                            (1.0+fixed_pars["eta"])*phi_d - 1.0)
                    modname = "CBD"
                elif k == "GHC":
                    t_coll = 10.**mcmc["chains"][w,-1,2]
                    pars = (eta, t_coll/t_sf)
                    modname = "GHC"
                elif k == "IE":
                    delta = mcmc["chains"][w,-1,2]
                    chi = t_sf / t_ff
                    pars = (eta, delta, chi)
                    modname = "IE"

                # Add dummy line for legend
                if w == 0 and j == 0:
                    plt.plot([-1,-1], [-1,-1], 'C{:d}'.format(ctr),
                             label=plot_labels[k])
                # Plot model
                plt.plot(np.log10(t), age_dist(tau_obs, t/t_sf, pars,
                                               modname, sigma, bias,
                                               log=True),
                         'C{:d}'.format(ctr), alpha=0.2)
            ctr = ctr+1


        # Add legend and adjust limits and labels
        if j == 0 or i == 0: plt.legend(loc='upper left')
        if j == 1 and i == 1: plt.legend(loc='upper left')
        plt.xlim(tlim)
        plt.ylim(ylim)
        if j == 0: plt.gca().set_xticklabels([])
        else: plt.xlabel("$\log\,t_{\mathrm{*,obs}}$ [Myr]")
        if i != 0: plt.gca().set_yticklabels([])
        else: plt.ylabel(r"$dp/d\log\,t_{\mathrm{*,obs}}$")
        if i == 1: plt.title(clname)

# Final adjustments
plt.subplots_adjust(hspace=0.25, wspace=0.1, right=0.95, top=0.95,
                    left=0.1)
plt.savefig('age_fits.pdf')


#############################################################

# Plot full posterior PDFs

plt.figure(2, figsize=(10,10))
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.clf()

# Labels to appears on axes
pars = [ { "label" : r"$\log\,t_{\mathrm{clust}}$ [Myr]",
           "lim"   : [-1, 2] },
         { "label" : r"$\log\,t_{\mathrm{sf}}$ [Myr]",
           "lim"   : [-1, 2] } ]
derived_pars = [ { "label" : r"$\log\,\epsilon_{\mathrm{ff}}$",
                   "lim"   : [-3, 0] },
                 { "label" : r"$f_{\mathrm{g,clust}}$",
                   "lim"   : [-4, 2] },
                 { "label" : r"$\log\,\epsilon_{\mathrm{*,clust}}$",
                   "lim"   : [-2, 0] } ]
model_pars = { "ST"   : [],
               "CB"   : [ { "label" : r"$\log\,t_{\mathrm{acc}}$ [Myr]",
                           "lim"   : [-2, 2] } ],
               "CBp0" : [ { "label" : r"$\log\,t_{\mathrm{acc}}$ [Myr]",
                            "lim"   : [-2, 2] } ],
               "CBD"  : [ { "label" : r"$\log\,t_{\mathrm{acc}}$ [Myr]",
                            "lim"   : [-2, 2] },
                          { "label" : r"$\log\,\phi_{\mathrm{d}}$",
                            "lim"   : [0, 2] } ],
               "GC"   : [ { "label" : r"$\log\,t_{\mathrm{coll}}$ [Myr]",
                            "lim"   : [-2, 2] },
                          { "label" : r"$\log\,\xi$",
                            "lim"   : [-1, 1] } ],
               "GCD"  : [ { "label" : r"$\log\,t_{\mathrm{coll}}$ [Myr]",
                            "lim"   : [-2, 2] },
                          { "label" : r"$\log\,t_{\mathrm{fb}}$ [Myr]",
                            "lim"   : [-2, 2] },
                          { "label" : r"$\log\,\phi_{\mathrm{d}}$",
                            "lim"   : [0, 2] },
                          { "label" : r"$\log\,\xi$",
                            "lim"   : [-1, 1] } ],
               "IE"   : [ { "label" : r"$\delta$",
                            "lim"   : [0, 3] } ] }

# Loop over clusters and models
for clname in clusters.keys():
    for modname in model_names:

        # Grab the MCMC outputs
        mcmc = clusters[clname]["mcmc"][modname]
        chains = mcmc["chains"]
        ndim = chains.shape[-1]
        samples = chains[:,nburn:,:].reshape(-1,ndim)

        # For the GC and GCD cases, add xi
        if modname == "GC" or modname == "GCD":
            logxi = np.log10(mcmc["xi"])
            samples = np.hstack((samples, logxi.reshape(logxi.size,1)))

        # Add derived quantities
        logeff = np.log10(mcmc["eff"])
        samples = np.hstack((samples, logeff.reshape(logeff.size,1)))
        fg = mcmc["fg"]
        samples = np.hstack((samples, fg.reshape(fg.size,1)))
        logepss = np.log10(mcmc["epss"])
        samples = np.hstack((samples, logepss.reshape(logepss.size,1)))

        # Set labels and limits for this model
        labels = [p["label"] for p in pars] + \
                 [mp["label"] for mp in model_pars[modname]] + \
                 [dp["label"] for dp in derived_pars]
        lolim = [p["lim"][0] for p in pars] + \
                [mp["lim"][0] for mp in model_pars[modname]] + \
                [dp["lim"][0] for dp in derived_pars]
        hilim = [p["lim"][1] for p in pars] + \
                [mp["lim"][1] for mp in model_pars[modname]] + \
                [dp["lim"][1] for dp in derived_pars]

        # Make plot
        npdfplot(samples, fignum=2, labels=labels,
                 histlim_rel=[0.01, 1.3])
        plt.subplots_adjust(top=0.96, left=0.1, hspace=0.12, wspace=0.12,
                            bottom=0.09)

        # Save
        plt.savefig(clname+"_"+modname+".pdf")
